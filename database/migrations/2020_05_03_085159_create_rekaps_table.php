<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekap', function (Blueprint $table) {
            $table->bigIncrements('id_rekap');
            $table->unsignedBigInteger('id_pegawai');
            $table->foreign('id_pegawai')->references('id_pegawai')->on('pegawai');
            $table->unsignedBigInteger('id_peserta');
            $table->foreign('id_peserta')->references('id_peserta')->on('peserta');
            $table->string('nilai_akhir', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rekap', function (Blueprint $table) {
            $table->dropForeign(['id_pegawai']);
            $table->dropForeign(['id_peserta']);
        });
        Schema::dropIfExists('rekap');
    }
}
