<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRekapDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekap_detail', function (Blueprint $table) {
            $table->bigIncrements('id_rekap_detail');
            $table->unsignedBigInteger('id_rekap');
            $table->foreign('id_rekap')->references('id_rekap')->on('rekap');
            $table->unsignedBigInteger('id_penilaian_detail');
            $table->foreign('id_penilaian_detail')->references('id_penilaian_detail')->on('penilaian_detail');
            $table->string('nilai_detail', 10);
            $table->string('ket', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rekap_detail', function (Blueprint $table) {
            $table->dropForeign(['id_penilaian_detail']);
            $table->dropForeign(['id_rekap']);
        });
        Schema::dropIfExists('rekap_detail');
    }
}
