<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaianDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_detail', function (Blueprint $table) {
            $table->bigIncrements('id_penilaian_detail');
            $table->unsignedBigInteger('id_penilaian_induk');
            $table->foreign('id_penilaian_induk')->references('id_penilaian_induk')->on('penilaian_induk');
            $table->string('nama_penilaian_detail', 200);
            $table->string('persentase_detail', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penilaian_detail', function (Blueprint $table) {
            $table->dropForeign(['id_penilaian_induk']);
        });
        Schema::dropIfExists('penilaian_detail');
    }
}
