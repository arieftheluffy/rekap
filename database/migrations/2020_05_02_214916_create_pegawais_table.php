<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->bigIncrements('id_pegawai');
            $table->string('nama_pegawai', 100);
            $table->string('nip', 18);
            $table->string('tempat_lahir', 100);
            $table->date('tgl_lahir');
            $table->enum('jk', ['L','P']);
            $table->string('unit_kerja', 100);
            $table->string('instansi', 100);
            $table->bigInteger('id_pangkat')->unsigned();
            $table->foreign('id_pangkat')->references('id')->on('pangkat');
            $table->bigInteger('id_jabatan')->unsigned();
            $table->foreign('id_jabatan')->references('id_jabatan')->on('jabatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pegawai', function (Blueprint $table) {
            $table->dropForeign(['id_pangkat']);
            $table->dropForeign(['id_jabatan']);
        });
        Schema::dropIfExists('pegawai');
    }
}
