<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelatihansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelatihan', function (Blueprint $table) {
            $table->bigIncrements('id_pelatihan');
            $table->unsignedBigInteger('id_jenis');
            $table->foreign('id_jenis')->references('id_jenis')->on('jenis');
            $table->unsignedBigInteger('id_lokasi');
            $table->foreign('id_lokasi')->references('id_lokasi')->on('lokasi');
            $table->string('nama_pelatihan', 200);
            $table->string('ket', 200)->nullable();
            $table->string('jp', 5);
            $table->string('kuota', 3);
            $table->string('tahun', 4);
            $table->string('tgl_mulai', 20);
            $table->string('tgl_akhir', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pelatihan', function (Blueprint $table) {
            $table->dropForeign(['id_jenis']);
            $table->dropForeign(['id_lokasi']);
        });

        Schema::dropIfExists('pelatihan');
    }
}
