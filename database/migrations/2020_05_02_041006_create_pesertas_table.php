<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peserta', function (Blueprint $table) {
            $table->bigIncrements('id_peserta');
            $table->string('nama_peserta', 100);
            $table->string('nip', 14);
            $table->string('nik', 16);
            $table->string('tempat_lahir');
            $table->string('alamat', 200);
            $table->bigInteger('id_agama')->unsigned();
            $table->foreign('id_agama')->references('id')->on('agama');
            $table->bigInteger('id_pangkat')->unsigned();
            $table->foreign('id_pangkat')->references('id')->on('pangkat');
            $table->bigInteger('id_pelatihan')->unsigned();
            $table->foreign('id_pelatihan')->references('id_pelatihan')->on('pelatihan');
            $table->string('tlp', 14);
            $table->string('instansi', 150);
            $table->date('tgl_lahir');
            $table->enum('jk', ['L', 'P']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peserta', function (Blueprint $table) {
            $table->dropForeign(['id_pangkat']);
            $table->dropForeign(['id_agama']);
        });
        Schema::dropIfExists('peserta');
    }
}
