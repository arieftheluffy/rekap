<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenilaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian_induk', function (Blueprint $table) {
            $table->bigIncrements('id_penilaian_induk');
            $table->string('nama_penilaian_induk', 150);
            $table->unsignedBigInteger('id_jenis');
            $table->foreign('id_jenis')->references('id_jenis')->on('jenis');
            $table->string('persentase', 5);
            $table->string('ket', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('penilaian_induk', function (Blueprint $table) {
            $table->dropForeign(['id_pelatihan']);
        });
        Schema::dropIfExists('penilaian_induk');
    }
}
