-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 22 Bulan Mei 2020 pada 15.47
-- Versi server: 5.7.24
-- Versi PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rekap_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agama`
--

CREATE TABLE `agama` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_agama` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `agama`
--

INSERT INTO `agama` (`id`, `nama_agama`, `created_at`, `updated_at`) VALUES
(1, 'Islam', '2020-05-01 15:41:53', '2020-05-01 15:41:53'),
(2, 'Kristen Katolik', '2020-05-01 15:44:54', '2020-05-01 15:44:54'),
(3, 'Kristen Protestan', '2020-05-01 15:47:08', '2020-05-01 15:47:08'),
(4, 'Hindu', '2020-05-01 15:48:53', '2020-05-01 15:48:53'),
(6, 'Budha', '2020-05-01 16:03:24', '2020-05-01 16:03:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` bigint(20) UNSIGNED NOT NULL,
  `nama_jabatan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `nama_jabatan`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'Widyaiswara Ahli Pertama', NULL, '2020-05-02 13:46:21', '2020-05-02 13:46:21'),
(2, 'Widyaiswara Ahli Muda', NULL, '2020-05-17 14:28:50', '2020-05-17 14:28:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` bigint(20) UNSIGNED NOT NULL,
  `jenis_pelatihan` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `jenis_pelatihan`, `ket`, `created_at`, `updated_at`) VALUES
(4, 'Pelatihan Dasar CPNS', 'Latsar', '2020-05-01 17:10:24', '2020-05-01 17:10:24'),
(5, 'Pelatihan Kepemimpinan Administrator', 'PKA', '2020-05-01 17:10:43', '2020-05-01 17:10:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` bigint(20) UNSIGNED NOT NULL,
  `nama_lokasi` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `nama_lokasi`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'BPSDM Provinsi Kaltim', NULL, '2020-05-01 17:49:37', '2020-05-01 17:49:37'),
(2, 'LPMP Samarinda', NULL, '2020-05-01 17:50:19', '2020-05-01 17:50:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2020_05_01_213224_create_agamas_table', 2),
(8, '2020_05_02_000517_create_pangkats_table', 3),
(9, '2020_05_02_004601_create_jenis_table', 4),
(10, '2020_05_02_011519_create_lokasis_table', 5),
(11, '2020_05_02_015450_create_pelatihans_table', 6),
(13, '2020_05_02_041006_create_pesertas_table', 7),
(14, '2020_05_02_213025_create_jabatans_table', 8),
(15, '2020_05_02_214916_create_pegawais_table', 9),
(18, '2020_05_03_031559_create_penilaians_table', 10),
(21, '2020_05_03_051007_create_penilaian__details_table', 11),
(24, '2020_05_03_085159_create_rekaps_table', 12),
(25, '2020_05_04_221618_create_rekap_details_table', 13);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pangkat`
--

CREATE TABLE `pangkat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pangkat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `golongan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pangkat`
--

INSERT INTO `pangkat` (`id`, `nama_pangkat`, `golongan`, `created_at`, `updated_at`) VALUES
(1, 'Juru Muda', 'I/a', '2020-05-01 16:33:01', '2020-05-01 16:33:01'),
(2, 'Juru Muda Tingkat I', 'I/b', '2020-05-01 16:35:47', '2020-05-01 16:35:47'),
(3, 'Juru', 'I/c', '2020-05-01 16:39:16', '2020-05-01 16:39:16'),
(4, 'Juru Tingkat I', 'I/d', '2020-05-01 16:39:41', '2020-05-01 16:39:41'),
(5, 'Pengatur Muda', 'II/a', '2020-05-17 05:34:10', '2020-05-17 05:34:10'),
(6, 'Pengatur Muda Tingkat I', 'II/b', '2020-05-17 05:35:26', '2020-05-17 05:35:26'),
(7, 'Pengatur', 'II/c', '2020-05-17 05:35:41', '2020-05-17 05:35:41'),
(8, 'Pengatur Tingkat I', 'II/d', '2020-05-17 05:35:58', '2020-05-17 05:35:58'),
(9, 'Penata Muda', 'III/a', '2020-05-17 05:36:12', '2020-05-17 05:36:12'),
(10, 'Penata Muda Tingkat I', 'III/b', '2020-05-17 05:36:41', '2020-05-17 05:36:41'),
(11, 'Penata', 'III/c', '2020-05-17 05:36:54', '2020-05-17 05:36:54'),
(12, 'Penata Tingkat I', 'III/d', '2020-05-17 05:37:40', '2020-05-17 05:37:40'),
(13, 'Pembina', 'IV/a', '2020-05-17 05:37:54', '2020-05-17 05:37:54'),
(14, 'Pembina Tingkat I', 'IV/b', '2020-05-17 05:38:06', '2020-05-17 05:38:06'),
(15, 'Pembina Utama Muda', 'IV/c', '2020-05-17 05:38:18', '2020-05-17 05:38:18'),
(16, 'Pembina Utama Madya', 'IV/d', '2020-05-17 05:38:44', '2020-05-17 05:38:44'),
(17, 'Pembina Utama', 'IV/e', '2020-05-17 05:38:56', '2020-05-17 05:38:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` bigint(20) UNSIGNED NOT NULL,
  `nama_pegawai` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_kerja` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instansi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_pangkat` bigint(20) UNSIGNED NOT NULL,
  `id_jabatan` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `tempat_lahir`, `tgl_lahir`, `jk`, `unit_kerja`, `instansi`, `id_pangkat`, `id_jabatan`, `created_at`, `updated_at`) VALUES
(2, 'Drs. Muchlis Syahrani, MBA.', '195911121980031007', 'Tenggarong', '1959-12-11', 'L', 'BPSDM Provinsi Kalimantan Timur', 'Pemerintah Provinsi Kalimantan Timur', 1, 1, '2020-05-02 14:51:47', '2020-05-02 14:51:47'),
(3, 'Imbran, M.Si.', '198212282011011001', 'Balikpapan', '1982-12-28', 'L', 'BPSDM Provinsi Kalimantan Timur', 'Pemerintah Provinsi Kalimantan Timur', 10, 2, '2020-05-17 14:29:50', '2020-05-17 14:29:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelatihan`
--

CREATE TABLE `pelatihan` (
  `id_pelatihan` bigint(20) UNSIGNED NOT NULL,
  `id_jenis` bigint(20) UNSIGNED NOT NULL,
  `id_lokasi` bigint(20) UNSIGNED NOT NULL,
  `nama_pelatihan` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jp` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kuota` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_mulai` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_akhir` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pelatihan`
--

INSERT INTO `pelatihan` (`id_pelatihan`, `id_jenis`, `id_lokasi`, `nama_pelatihan`, `ket`, `jp`, `kuota`, `tahun`, `tgl_mulai`, `tgl_akhir`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 'Pelatihan Dasar CPNS Angkatan I Kota Bontang', 'LATSAR', '511', '40', '2020', '2020-01-02', '2020-03-02', '2020-05-01 19:22:28', '2020-05-21 23:45:35'),
(2, 4, 1, 'Pelatihan Dasar CPNS Angkatan II Kota Bontang', 'LATSAR', '511', '40', '2020', '2020-01-02', '2020-03-02', '2020-05-02 08:17:13', '2020-05-18 14:27:26'),
(3, 5, 1, 'Pelatihan Kepemimpinan Angkatan I Kelas Gabungan', 'PKA', '811', '40', '2020', '2020-01-01', '2020-03-03', '2020-05-04 14:51:27', '2020-05-19 14:34:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian_detail`
--

CREATE TABLE `penilaian_detail` (
  `id_penilaian_detail` bigint(20) UNSIGNED NOT NULL,
  `id_penilaian_induk` bigint(20) UNSIGNED NOT NULL,
  `nama_penilaian_detail` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `persentase_detail` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `penilaian_detail`
--

INSERT INTO `penilaian_detail` (`id_penilaian_detail`, `id_penilaian_induk`, `nama_penilaian_detail`, `persentase_detail`, `ket`, `created_at`, `updated_at`) VALUES
(17, 3, 'Ketepatan rencana aksi perubahan', '5', 'Kemampuan melakukan analisis untuk mendapatkan ketepatan Rencana Aksi Perubahan dengan permasalahan kualitas atau peningkatan kualitas kinerja organisasi', '2020-05-07 05:48:21', '2020-05-07 05:48:21'),
(18, 3, 'Terobosan inovatif', '5', 'Kemampuan memecahkan masalah yang berkaitan dengan kinerja organisasi secara inovatif sesuai dengan kriteria inovasi: (a) memberi nilai tambah bagi organisasi dan stakeholder, (b) memiliki unsur kebaharuan, (c) bisa direplikasi, dan (d) dapat diterapkan secara berkelanjutan, dan (e) sesuai dengan nilai-nilai organisasi', '2020-05-07 05:48:46', '2020-05-07 05:48:46'),
(19, 3, 'Tahapan rencana perubahan dan pengendalian resiko', '5', 'Kemampuan menyusun keterkaitan antara tahapan rencana aksi perubahan dan pengendalian risiko untuk mendapatkan hasil yang diharapkan', '2020-05-07 05:49:30', '2020-05-07 05:49:30'),
(20, 3, 'Kejelasan peta dan pemanfaatan sumberdaya organisasi', '5', 'Kemampuan mengidentifikasi dan menjelaskan rencana pemanfaatan sumberdaya organisasi terdiri atas 1). tim kerja, 2). jejaring kerja, dan', '2020-05-07 05:50:19', '2020-05-07 05:50:19'),
(21, 5, 'Capaian hasil terhadap rencana perubahan', '8', 'Kemampuan menjelaskan capaian implementasi rencana aksi perubahan yang didukung dengan bukti-bukti yang valid dan relevan meliputi', '2020-05-19 15:41:55', '2020-05-19 15:41:55'),
(22, 5, 'Kepemimpinan', '8', 'Kemampuan mengaktualisasikan kepemimpinan transformasional dalam pelaksanaan aksi perubahan untuk memperoleh hasil yang diharapkan meliputi', '2020-05-19 15:42:23', '2020-05-19 15:42:23'),
(23, 5, 'Kemanfaatan Aksi Perubahan', '7', 'Cakupan manfaat aksi perubahan terhadap peningkatan kualitas kinerja organisasi saat ini dan yang akan datang meliputi', '2020-05-19 15:42:52', '2020-05-19 15:42:52'),
(24, 5, 'Keberlanjutan Aksi Perubahan', '7', 'Kemampuan peserta dalam menunjukan upaya yang menjamin keberlanjutan aksi perubahan meliputi', '2020-05-19 15:43:14', '2020-05-19 15:43:14'),
(25, 1, 'Kemampuan menganalisis konsep dalam agenda kepemimpinan kinerja', '6', 'PKA', '2020-05-21 21:14:36', '2020-05-21 21:14:36'),
(26, 1, 'Kemampuan menganalisis konsep dalam agenda manajemen kinerja', '9', 'PKA', '2020-05-21 21:14:55', '2020-05-21 21:14:55'),
(27, 2, 'Kualitas dokumentasi lesson learnt Studi Lapangan yang didukung dengan data dan informasi yang relevan (kelompok)', '10', NULL, '2020-05-21 21:16:24', '2020-05-21 21:16:24'),
(28, 2, 'Kualitas lesson learnt adopsi dan adaptasi hasil Studi Lapangan (individual) sesuai agenda pembelajaran', '10', NULL, '2020-05-21 21:16:39', '2020-05-21 21:16:39'),
(29, 6, 'Kepemimpinan', '5', NULL, '2020-05-21 21:22:43', '2020-05-21 21:22:43'),
(30, 6, 'Kerjasama', '5', NULL, '2020-05-21 21:22:57', '2020-05-21 21:22:57'),
(31, 6, 'Kedisiplinan', '5', NULL, '2020-05-21 21:23:19', '2020-05-21 21:23:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian_induk`
--

CREATE TABLE `penilaian_induk` (
  `id_penilaian_induk` bigint(20) UNSIGNED NOT NULL,
  `nama_penilaian_induk` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jenis` bigint(20) UNSIGNED NOT NULL,
  `persentase` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket_induk` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `penilaian_induk`
--

INSERT INTO `penilaian_induk` (`id_penilaian_induk`, `nama_penilaian_induk`, `id_jenis`, `persentase`, `ket_induk`, `created_at`, `updated_at`) VALUES
(1, 'Evaluasi Substansi', 5, '15', 'PKA', '2020-05-02 20:22:39', '2020-05-02 20:22:39'),
(2, 'Evaluasi Studi Lapangan', 5, '20', 'PKA', '2020-05-02 20:39:46', '2020-05-02 20:39:46'),
(3, 'Evaluasi Perencanaan Aksi Perubahan', 5, '20', 'PKA', '2020-05-03 06:14:18', '2020-05-03 06:14:18'),
(5, 'Evaluasi Implementasi Aksi Perubahan', 5, '30', 'PKA', '2020-05-19 15:40:22', '2020-05-19 15:40:22'),
(6, 'Evaluasi Sikap Perilaku', 5, '15', NULL, '2020-05-21 21:22:17', '2020-05-21 21:22:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peserta`
--

CREATE TABLE `peserta` (
  `id_peserta` bigint(20) UNSIGNED NOT NULL,
  `nama_peserta` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_agama` bigint(20) UNSIGNED NOT NULL,
  `id_pangkat` bigint(20) UNSIGNED NOT NULL,
  `id_pelatihan` bigint(20) UNSIGNED NOT NULL,
  `tlp` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instansi` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `peserta`
--

INSERT INTO `peserta` (`id_peserta`, `nama_peserta`, `nip`, `nik`, `tempat_lahir`, `alamat`, `id_agama`, `id_pangkat`, `id_pelatihan`, `tlp`, `instansi`, `tgl_lahir`, `jk`, `created_at`, `updated_at`) VALUES
(115, 'Adrianus Joni, S.H., M.M.', '197907132005021005', '6407081307790002', 'Kutai Barat', 'Kampung Benung RT.001 Kecamatan Damai Kabupaten Kutai Barat', 2, 13, 3, '0542594754', 'Pemerintah Kabupaten Kutai Barat', '1979-07-13', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(116, 'Christina Yacob, S.Kom,. M.Si', '198203262010012016', '6407076603820002', 'Tenggarong', 'Jln. Gajah mada Rt. 4 Barong Tongkok', 2, 14, 3, '0545594756', 'Pemerintah Kabupaten Kutai Barat', '1982-03-26', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(117, 'H. Zairin Handani, S.E., M.Si.', '197301012007011044', '6407070101730002', 'Melak', 'Jl. Dewi Sartika  RT. 3 No. 67  Kelurahan Barong Tongkok. Kecamatan Barong Tongkok  Kabupaten Kutai Barat 75776', 1, 11, 3, '0542594754', 'Pemerintah Kabupaten Kutai Barat', '1973-01-01', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(118, 'Sri Sedono Iswandi S.K.M., M.Kes.', '196710171990031007', '6472051710670001', 'Pacitan', 'Jl. Perjuangan, alam segar 3 no. 1 Samarinda', 1, 13, 3, '(0541) 743908 - 7072484', 'Pemerintah Provinsi Kalimantan Timur', '1967-10-17', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(119, 'drg. Shanty Sintessa Wulaningrum M.Kes.', '197210032000122003', '6472024310720003', 'Pati', 'PERUM GEMILANG BLOK. LA-143 Rapak Dalam SAMARINDA SEBERANG 75231', 1, 13, 3, '(0541) 743908 - 7072484', 'Pemerintah Provinsi Kalimantan Timur', '1972-10-03', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(120, 'Hadi Machbudiansyah S.E.', '197509111994021001', '6472041109750005', 'Samarinda', 'Jl. Damai RT.28 Sidodamai Samarinda', 1, 14, 3, '(0541) 743364', 'Pemerintah Provinsi Kalimantan Timur', '1975-09-11', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(121, 'Teofilus Budi, S.E.', '197903052007011011', '6407150503790001', 'Barong Tongkok', 'Kampung Juaq Asa', 2, 11, 3, '0541-666666', 'Pemerintah Kabupaten Kutai Barat', '1979-03-05', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(122, 'Akhmat Yani, S.Sos.', '196806141986101001', '6403051406680000', 'Tanjung Redeb', 'Jl. H. Abdullah Rt. 07 Kelurahan Karang Ambon, Kecamatan  Tanjung Redeb', 1, 14, 3, '0554-2022145', 'Pemerintah Kabupaten Berau', '1968-06-14', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(123, 'H .Fitriansyah S.T., M.M.', '197311272006041009', '6472052711730002', 'Samarinda', 'JL. KEMAKMURAN KOMPLEK PELITA 3 NO. 43 RT 41 SAMARINDA 75117', 1, 15, 3, '(0541) 7270208', 'Pemerintah Provinsi Kalimantan Timur', '1973-11-27', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(124, 'I Putu Budiasa, S.Pd.', '197007251993061001', '6407122507700001', 'Buleleng', 'Buara Gusik', 4, 14, 3, '0541-666666', 'Pemerintah Kabupaten Kutai Barat', '1970-07-25', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(125, 'Fahmi Himawan S.T., M.T.', '197412162006041006', '6472051612740014', 'Samarinda', 'Perum Bumi Sempaja Blok FD/01, RT.046, Kel.Sempaja Selatan, Kec.Samarinda Utara, Kota Samarinda 75119', 1, 13, 3, '(0541) 760304', 'Pemerintah Provinsi Kalimantan Timur', '1974-12-16', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(126, 'A. Rafiq S.Sos., M.Si.', '197203151998031010', '6472051503720007', 'Samarinda', 'Jln. Bengkuring Blok E No. 297 RT. 092 Samarinda Kaltim', 1, 13, 3, '(0541) 733766 - 741040', 'Pemerintah Provinsi Kalimantan Timur', '1972-03-15', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(127, 'Ananias, S.Hut., MP.', '198101152005021003', '6402041501810001', 'Kutai Kartanegara', 'Sidorukun', 2, 14, 3, '0541-666666', 'Pemerintah Kabupaten Kutai Kartanegara', '1981-01-15', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(128, 'Hesbend Nafsiah Nuransyah, S.Sos.', '196907231995031002', '6403052307690003', 'Berau', 'Jl. P. Diponogoro I RT. 11 Kel. Gn. Panjang Tanjung Redeb', 1, 13, 3, '-', 'Pemerintah Kabupaten Berau', '1969-07-23', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(129, 'Rahmayani John, S.K.M. M.Kes.', '197606242003122011', '6407206406760001', 'Mancong', 'Perum Korpri blok j no 3 sekolaq oday', 3, 13, 3, '4046885', 'Pemerintah Kabupaten Kutai Barat', '1976-06-24', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(130, 'Rakhmadani Hidayat, S.IP., M.Si.', '198804292007011003', '6402122904881002', 'Tenggarong', 'Jl. Gunung Belah Gg. Kita Jua', 1, 11, 3, '0541-666666', 'Pemerintah Kabupaten Kutai Kartanegara', '1988-03-04', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(131, 'dr. Eryariyatin, M.Kes.', '197903272006042009', '6472036703790001', 'Kuala Kapuas', 'Villa Tamara Blok. J No. 12 A, Jl. Aw. Syahranie', 1, 13, 3, '0541-666666', 'Pemerintah Kabupaten Kutai Kartanegara', '1979-03-27', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(132, 'dr. Akbar, M.Si.', '197502272002121008', '6407072702750001', 'Barru', 'Jl. R.A Kartini RT. 003 Barong Tongkok', 1, 13, 3, '0545-4044002', 'Pemerintah Kabupaten Kutai Barat', '1975-02-27', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(133, 'Aji Ali Husni AB, S.E., M.Si.', '197606061998031008', '6402060606760006', 'Tenggarong', 'Jl. Gunung Menyapa, No. 59, RT. 33', 1, 13, 3, '0541-666666', 'Pemerintah Kabupaten Kutai Kartanegara', '1976-06-06', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(134, 'Anthoni Safarisa S.E., M.M.', '197403122002121008', '6472031203740005', 'Samarinda', 'Jl. AW. Syahranie - Villa Tamara K1/2BSamarinda', 1, 13, 3, '(0541) 734969', 'Pemerintah Provinsi Kalimantan Timur', '1974-03-12', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(135, 'H. Irhan Hukmaidy S.Pi, M.P', '197512282000031002', '6472052812750002', 'Banjarmasin', 'Jl. Pakis Merah 15 D/570A RT.045 Kelurahan Sempaja Timur Kec. Samarinda Utara Samarinda', 1, 13, 3, '(0541) 743506', 'Pemerintah Provinsi Kalimantan Timur', '1975-12-28', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(136, 'Lorensius Itang, S.E.', '197501032007011013', '6407110301750003', 'Tanjung Isuy', 'Jl. Taman Siswa RT. 08 Kel. Tanjung Isuy Kec. Jempang', 2, 11, 3, '0545-4044002', 'Pemerintah Kabupaten Kutai Barat', '1975-01-03', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(137, 'Christianus Arie Dedy Bang, S.E., M,Si.', '197704082007011013', '6407070804770001', 'Tering', 'Ujoh Bilang RT. 11 Long Bagun', 2, 11, 3, '0545-4044001', 'Pemerintah Kabupaten Mahakam Ulu', '1977-04-08', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(138, 'Taupik Rahman S.Sos., M.M.', '197411241994031007', '6472061102090009', 'Samarinda', 'Jl. Pangeran Antasari Gg. Kenanga No. 26 RT. 5 Samarinda', 1, 13, 3, '(0541) 733333 - 737762', 'Pemerintah Provinsi Kalimantan Timur', '1974-11-24', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(139, 'Drs. Edi Hermawanto Noor M.Si.', '196903031991031012', '6472050303690005', 'Samarinda', 'Perum Solong Durian Blok A8 No.15 Samarinda Provinsi Kalimantan Timur', 1, 13, 3, '0541733333-737762', 'Pemerintah Provinsi Kalimantan Timur', '1969-03-03', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(140, 'Adji Yudhistira S.E., M.Si.', '197308141998031006', '6472031408730003', 'Samarinda', 'Jl. Biola No. 13 Rt. 2 Rw. 10 Kel. Dadi Mulya Kec. Samarinda Uli', 1, 13, 3, '(0541) 733333', 'Pemerintah Provinsi Kalimantan Timur', '1973-08-14', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(141, 'Satya Pambudi S.T., M.Si.', '197604302001121004', '6472043004760002', 'Samarinda', 'JL. Danau Maninjau No. 47 RT. 14 Sungai Pinang Luar Kec. Samarinda Kota, Samarinda, Kalimantan Timur', 1, 13, 3, '(0541) 733333', 'Pemerintah Provinsi Kalimantan Timur', '1976-04-30', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(142, 'Ardiansyah, S.Sos.,M.Si.', '196704301990021002', '6472063004670001', 'Kota Bangun', 'Jl. Slamet Riyadi Gg. Manunggal VI/7 RT. 39 Kel. Teluk Lerong Ulu Kec. Sungai Kunjang Samarinda', 1, 13, 3, '0541-733333 pes 148, 248', 'Pemerintah Provinsi Kalimantan Timur', '1967-04-30', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(143, 'Masmansur, S.T., M.Si', '196906152000121004', '6403051506690004', 'Bone', 'Jl. karang ambun', 1, 13, 3, '0541661616', 'Pemerintah Kabupaten Berau', '1969-06-15', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(144, 'Novita Retno Damayanti, S.K.M', '197111221997032004', '6471056211710004', 'Surabaya', 'Villa Damai Permai Blok. D 5-12 RT. 32 Kel. Gunung Bahagia Kec. Balikpapan Selatan Kota Balikpapan', 1, 14, 3, '0542-873539', 'Pemerintah Provinsi Kalimantan Timur', '1971-11-22', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(145, 'Heni Purwaningsih S.Si., M.Si.', '197303141998032009', '6402035403730001', 'Blitar', 'Manunggal, I 24 22 Loa Janan Ulu Kutai 75391', 1, 13, 3, '(0541) 742482', 'Pemerintah Provinsi Kalimantan Timur', '1973-03-14', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(146, 'Saprudin Saida Panda, S.H., M.Si.', '197003132000121002', '6472051303700004', 'Buton', 'Jl. Batu Cermin Rt.5 Kel. Sempaja Utara Kec. Samarinda Utara', 1, 13, 3, '0541-732179', 'Pemerintah Provinsi Kalimantan Timur', '1970-03-13', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(147, 'Syarifah Vanda Savitry Syafril, M.Si.', '196709221992032005', '6402066809670001', 'Tenggarong', 'KH. Wahid Hasyim II Perum. Kayu Manis Blok. B.17 Sempaja', 1, 13, 3, '0541-205476', 'Pemerintah Provinsi Kalimantan Timur', '1967-09-22', 'P', '2020-05-17 05:43:21', '2020-05-17 05:43:21'),
(148, 'Dedy Wahyudi, S.E.,M.Si.', '196906222001121002', '6402062206690001', 'Tenggarong', 'Jl. Mawar RT. 013 Kel. Sukarame, Tenggarong', 1, 13, 3, '0541-661090', 'Pemerintah Kabupaten Kutai Kartanegara', '1969-06-22', 'L', '2020-05-17 05:43:21', '2020-05-17 05:43:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekap`
--

CREATE TABLE `rekap` (
  `id_rekap` bigint(20) UNSIGNED NOT NULL,
  `id_pegawai` bigint(20) UNSIGNED NOT NULL,
  `id_peserta` bigint(20) UNSIGNED NOT NULL,
  `id_penilaian_detail` bigint(20) UNSIGNED NOT NULL,
  `nilai_detail` int(11) NOT NULL,
  `nilai_akhir` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `ket` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `rekap`
--

INSERT INTO `rekap` (`id_rekap`, `id_pegawai`, `id_peserta`, `id_penilaian_detail`, `nilai_detail`, `nilai_akhir`, `ket`, `created_at`, `updated_at`) VALUES
(29, 3, 124, 17, 90, '4.5', NULL, '2020-05-17 14:27:26', '2020-05-17 14:27:26'),
(30, 3, 124, 18, 97, '4.85', NULL, '2020-05-17 14:30:06', '2020-05-17 14:30:40'),
(33, 3, 115, 19, 97, '4.85', NULL, '2020-05-18 14:42:01', '2020-05-18 14:42:01'),
(35, 3, 115, 17, 76, '3.8', NULL, '2020-05-19 09:49:54', '2020-05-19 11:42:20'),
(36, 3, 115, 18, 78, '3.9', NULL, '2020-05-19 10:00:07', '2020-05-19 10:00:07'),
(37, 3, 115, 20, 90, '4.5', NULL, '2020-05-19 12:23:22', '2020-05-19 12:23:22'),
(39, 3, 115, 22, 98, '7.84', NULL, '2020-05-19 15:44:13', '2020-05-19 15:44:13'),
(40, 3, 115, 23, 94, '6.58', NULL, '2020-05-19 15:44:38', '2020-05-19 15:44:38'),
(41, 3, 115, 24, 97, '6.79', NULL, '2020-05-19 15:44:57', '2020-05-21 09:07:36'),
(43, 3, 116, 21, 96, '7.68', NULL, '2020-05-20 16:24:59', '2020-05-21 09:13:12'),
(48, 3, 115, 21, 90, '7.2', NULL, '2020-05-21 09:11:38', '2020-05-21 09:11:38'),
(49, 3, 116, 22, 78, '6.24', NULL, '2020-05-21 09:13:27', '2020-05-21 09:13:27'),
(50, 3, 116, 23, 90, '6.3', NULL, '2020-05-21 13:59:50', '2020-05-21 13:59:50'),
(51, 3, 116, 24, 90, '6.3', NULL, '2020-05-21 14:13:17', '2020-05-21 14:13:17'),
(52, 3, 117, 17, 78, '3.9', NULL, '2020-05-21 19:23:12', '2020-05-21 19:23:12'),
(53, 3, 115, 25, 98, '5.88', NULL, '2020-05-21 21:17:19', '2020-05-22 15:20:20'),
(54, 3, 115, 26, 90, '8.1', NULL, '2020-05-21 21:17:28', '2020-05-21 21:17:28'),
(55, 3, 115, 27, 99, '9.9', NULL, '2020-05-21 21:17:56', '2020-05-22 15:20:16'),
(56, 3, 115, 28, 90, '9', NULL, '2020-05-21 21:18:33', '2020-05-21 21:18:33'),
(57, 3, 115, 29, 99, '4.95', NULL, '2020-05-21 21:23:39', '2020-05-21 21:23:39'),
(58, 3, 115, 30, 90, '4.5', NULL, '2020-05-21 21:23:48', '2020-05-22 15:20:08'),
(59, 3, 115, 31, 97, '4.85', NULL, '2020-05-21 21:23:58', '2020-05-21 21:23:58'),
(60, 2, 118, 25, 90, '5.4', NULL, '2020-05-21 23:03:45', '2020-05-21 23:03:45'),
(61, 3, 116, 17, 80, '4', NULL, '2020-05-22 13:28:55', '2020-05-22 13:28:55'),
(62, 3, 121, 25, 90, '5.4', NULL, '2020-05-22 15:19:20', '2020-05-22 15:19:20'),
(63, 3, 116, 18, 90, '4.5', NULL, '2020-05-22 15:22:11', '2020-05-22 15:22:11'),
(64, 3, 116, 19, 99, '4.95', NULL, '2020-05-22 15:22:29', '2020-05-22 15:22:29'),
(65, 3, 116, 20, 90, '4.5', NULL, '2020-05-22 15:22:37', '2020-05-22 15:22:37'),
(66, 3, 116, 25, 85, '5.1', NULL, '2020-05-22 15:22:52', '2020-05-22 15:40:07'),
(67, 3, 116, 26, 90, '8.1', NULL, '2020-05-22 15:23:01', '2020-05-22 15:23:01'),
(68, 3, 116, 27, 90, '9', NULL, '2020-05-22 15:23:13', '2020-05-22 15:39:40'),
(69, 3, 116, 28, 98, '9.8', NULL, '2020-05-22 15:23:21', '2020-05-22 15:32:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekap_detail`
--

CREATE TABLE `rekap_detail` (
  `id_rekap_detail` bigint(20) UNSIGNED NOT NULL,
  `id_rekap` bigint(20) UNSIGNED NOT NULL,
  `id_penilaian_detail` bigint(20) UNSIGNED NOT NULL,
  `nilai_detail` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('staff','admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `level`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Miftahul Arif Hidayah', 'admin', 'arieftheluffy@gmail.com', NULL, '$2y$10$pEB4hDH4qBB8uJaw.OV4M.PBYKVKPK028nompoGmpa4BL.pOc.eZC', NULL, '2020-05-01 06:34:21', '2020-05-01 06:34:21'),
(2, 'user', 'staff', 'user@gmail.com', NULL, '$2y$10$sJu741kzNLmXVWZFp66tT.Q1Ru.IJFijA59HXQ6uSF/FpgXs43dDe', NULL, '2020-05-18 14:30:04', '2020-05-18 14:30:04');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pangkat`
--
ALTER TABLE `pangkat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`),
  ADD KEY `pegawai_id_pangkat_foreign` (`id_pangkat`),
  ADD KEY `pegawai_id_jabatan_foreign` (`id_jabatan`);

--
-- Indeks untuk tabel `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD PRIMARY KEY (`id_pelatihan`),
  ADD KEY `pelatihan_id_jenis_foreign` (`id_jenis`),
  ADD KEY `pelatihan_id_lokasi_foreign` (`id_lokasi`);

--
-- Indeks untuk tabel `penilaian_detail`
--
ALTER TABLE `penilaian_detail`
  ADD PRIMARY KEY (`id_penilaian_detail`),
  ADD KEY `penilaian_detail_id_penilaian_induk_foreign` (`id_penilaian_induk`);

--
-- Indeks untuk tabel `penilaian_induk`
--
ALTER TABLE `penilaian_induk`
  ADD PRIMARY KEY (`id_penilaian_induk`),
  ADD KEY `penilaian_induk_id_jenis_foreign` (`id_jenis`);

--
-- Indeks untuk tabel `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`id_peserta`),
  ADD KEY `peserta_id_agama_foreign` (`id_agama`),
  ADD KEY `peserta_id_pangkat_foreign` (`id_pangkat`),
  ADD KEY `peserta_id_pelatihan_foreign` (`id_pelatihan`);

--
-- Indeks untuk tabel `rekap`
--
ALTER TABLE `rekap`
  ADD PRIMARY KEY (`id_rekap`),
  ADD KEY `rekap_id_pegawai_foreign` (`id_pegawai`),
  ADD KEY `rekap_id_peserta_foreign` (`id_peserta`),
  ADD KEY `id_penilaian_detail` (`id_penilaian_detail`);

--
-- Indeks untuk tabel `rekap_detail`
--
ALTER TABLE `rekap_detail`
  ADD PRIMARY KEY (`id_rekap_detail`),
  ADD KEY `rekap_detail_id_penilaian_detail_foreign` (`id_penilaian_detail`),
  ADD KEY `rekap_detail_id_rekap_foreign` (`id_rekap`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agama`
--
ALTER TABLE `agama`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id_lokasi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `pangkat`
--
ALTER TABLE `pangkat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pelatihan`
--
ALTER TABLE `pelatihan`
  MODIFY `id_pelatihan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `penilaian_detail`
--
ALTER TABLE `penilaian_detail`
  MODIFY `id_penilaian_detail` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `penilaian_induk`
--
ALTER TABLE `penilaian_induk`
  MODIFY `id_penilaian_induk` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id_peserta` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT untuk tabel `rekap`
--
ALTER TABLE `rekap`
  MODIFY `id_rekap` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT untuk tabel `rekap_detail`
--
ALTER TABLE `rekap_detail`
  MODIFY `id_rekap_detail` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id_jabatan`),
  ADD CONSTRAINT `pegawai_id_pangkat_foreign` FOREIGN KEY (`id_pangkat`) REFERENCES `pangkat` (`id`);

--
-- Ketidakleluasaan untuk tabel `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD CONSTRAINT `pelatihan_id_jenis_foreign` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  ADD CONSTRAINT `pelatihan_id_lokasi_foreign` FOREIGN KEY (`id_lokasi`) REFERENCES `lokasi` (`id_lokasi`);

--
-- Ketidakleluasaan untuk tabel `penilaian_detail`
--
ALTER TABLE `penilaian_detail`
  ADD CONSTRAINT `penilaian_detail_id_penilaian_induk_foreign` FOREIGN KEY (`id_penilaian_induk`) REFERENCES `penilaian_induk` (`id_penilaian_induk`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penilaian_induk`
--
ALTER TABLE `penilaian_induk`
  ADD CONSTRAINT `penilaian_induk_id_jenis_foreign` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`);

--
-- Ketidakleluasaan untuk tabel `peserta`
--
ALTER TABLE `peserta`
  ADD CONSTRAINT `peserta_id_agama_foreign` FOREIGN KEY (`id_agama`) REFERENCES `agama` (`id`),
  ADD CONSTRAINT `peserta_id_pangkat_foreign` FOREIGN KEY (`id_pangkat`) REFERENCES `pangkat` (`id`),
  ADD CONSTRAINT `peserta_id_pelatihan_foreign` FOREIGN KEY (`id_pelatihan`) REFERENCES `pelatihan` (`id_pelatihan`);

--
-- Ketidakleluasaan untuk tabel `rekap`
--
ALTER TABLE `rekap`
  ADD CONSTRAINT `rekap_ibfk_1` FOREIGN KEY (`id_penilaian_detail`) REFERENCES `penilaian_detail` (`id_penilaian_detail`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rekap_id_pegawai_foreign` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`),
  ADD CONSTRAINT `rekap_id_peserta_foreign` FOREIGN KEY (`id_peserta`) REFERENCES `peserta` (`id_peserta`);

--
-- Ketidakleluasaan untuk tabel `rekap_detail`
--
ALTER TABLE `rekap_detail`
  ADD CONSTRAINT `rekap_detail_id_penilaian_detail_foreign` FOREIGN KEY (`id_penilaian_detail`) REFERENCES `penilaian_detail` (`id_penilaian_detail`) ON DELETE CASCADE,
  ADD CONSTRAINT `rekap_detail_id_rekap_foreign` FOREIGN KEY (`id_rekap`) REFERENCES `rekap` (`id_rekap`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
