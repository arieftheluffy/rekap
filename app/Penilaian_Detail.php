<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian_Detail extends Model
{
    protected $table = "penilaian_detail";
    protected $primaryKey = "id_penilaian_detail";
    protected $fillable = [
        'nama_penilaian_detail', 'persentase_detail', 'id_penilaian_induk', 'ket'
    ];

    public function penilaian_induk()
    {
        return $this->belongsTo('App\Penilaian', 'id_penilaian_induk');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }
}