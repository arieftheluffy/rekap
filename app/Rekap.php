<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekap extends Model
{
    protected $table = "rekap";
    protected $primaryKey = "id_rekap";
    protected $fillable = [
        'id_peserta', 'id_pegawai', 'id_penilaian_detail', 'nilai_akhir', 'nilai_detail', 'ket'
    ];

    public function penilaiandetail()
    {
        return $this->belongsTo('App\Penilaian_Detail', 'id_penilaian_detail');
    }

    public function peserta()
    {
        return $this->belongsTo('App\Peserta', 'id_peserta');
    }
}
