<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = "pegawai";
    protected $primaryKey = "id_pegawai";
    protected $fillable = [
        'nama_pegawai','nip', 'tempat_lahir', 'tgl_lahir','jk','unit_kerja', 'instansi', 'id_pangkat', 'id_jabatan'
    ];
}
