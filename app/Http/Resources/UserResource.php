<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'name' => $this->name,
            'level' => $this->level,
            // 'remember_token' =>$this->remember_token
        ];
    }
}
