<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PelatihanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_pelatihan'=>$this->id_pelatihan,
            'id_jenis'=>$this->id_jenis,
            'id_lokasi'=>$this->id_lokasi,
            'nama_pelatihan'=>$this->nama_pelatihan,
            'ket'=>$this->ket,
            'jp'=>$this->jp,
            'kuota'=>$this->kuota,
            'tahun'=>$this->tahun,
            'tgl_mulai'=>$this->tgl_mulai,
            'tgl_akhir'=>$this->tgl_akhir,
        ];
    }
}
