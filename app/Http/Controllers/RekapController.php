<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Pelatihan;
use App\Penilaian;
use App\Penilaian_Detail;
use App\Peserta;
use App\Pangkat;
use App\Agama;
use App\Exports\NilaiExport;
use App\Rekap;
use App\Jenis;
use DB;
use Dompdf\FontMetrics;
use Dompdf\Options;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Validator;
use Illuminate\Support\Facades\URL;

class RekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $list_peserta = Peserta::all();
        $list_pelatihan = Pelatihan::all();
        // dump($list_pelatihan);
        return view ('rekapitulasi.index2', [
            'list_peserta' => $list_peserta,
            'list_pelatihan' => $list_pelatihan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function data_peserta($id)
    {
        $list_pelatihan = Pelatihan::findOrFail($id);

        $list_peserta = DB::table('peserta')
                            ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
                            ->WHERE('pelatihan.id_pelatihan', $id)
                            ->GET();
        // dump($list_peserta);
        return view ('rekapitulasi.data_nilai', [
            'list_peserta' => $list_peserta,
            'list_pelatihan' => $list_pelatihan,
        ]);
    }

    public function data_peserta_show($id)
    {
        // return $id;
        $list_pelatihan = Pelatihan::find($id);
        $list_peserta1 = Peserta::where('id_peserta', $id)->get();
        $id_pelatihan = $list_peserta1[0]->id_pelatihan;
        $list_pelatihan = Pelatihan::where('id_pelatihan', $id_pelatihan)->get();

        $list_peserta1 = DB::table('rekap')
            ->JOIN('peserta','peserta.id_peserta', '=','peserta.id_peserta')
            ->JOIN('penilaian_detail','penilaian_detail.id_penilaian_detail', '=','penilaian_detail.id_penilaian_detail')
            ->WHERE('peserta.id_peserta', $id)
            ->GET();

        $rekap = Rekap::where('id_peserta', $id)->get();

        //array ke chart
        $kategori = [];
        $datanilai = [];
        foreach($rekap as $a) {
            $kategori[] = $a->penilaiandetail->nama_penilaian_detail;
            $datanilai[] = $a->nilai_detail;
        }

        // return $datanilai;

        // return $rekap;
        return view ('rekapitulasi.data_nilai_show', [
            'list_peserta1' => $list_peserta1,
            'list_pelatihan' => $list_pelatihan,
            'rekap' => $rekap,
            'kategori' => $kategori,
            'datanilai' => $datanilai,
        ]);
    }

    public function create_nilai(Request $request)
    {
        $list_peserta = Peserta::all();
        $list_pelatihan = Pelatihan::all();
        // dump($list_pelatihan);
        return view ('rekapitulasi.create_nilai', [
            'list_peserta' => $list_peserta,
            'list_pelatihan' => $list_pelatihan
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validasi = Validator::make($input, [
            'id_peserta' => 'required',
            'id_pegawai' => 'required|max:200',
            'id_penilaian_induk' => 'required',
            'nilai' => 'required|max:100|numeric',
            'ket' => 'required|max:200'
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        // Rekap::create($input);

        return redirect()->route('rekapitulasi.index')->with('toast_success', 'Data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list_pelatihan = Pelatihan::find($id);

        $list_peserta = DB::table('peserta')
                            ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
                            ->WHERE('pelatihan.id_pelatihan', $id)
                            ->GET();
        // dump($list_peserta);
        return view ('rekapitulasi.pelatihan', [
            'list_peserta' => $list_peserta,
            'list_pelatihan' => $list_pelatihan,
        ]);
    }
    public function simpan_sementara(Request $request)
    {
        // $all = $request->all();
        // // dump($all);
        // $id_penilaian_induk = $request->input('penilaian_induk');
        // $nama_penilaian_detail = Penilaian_Detail::where('id_penilaian_induk', '=', $id_penilaian_induk)->get();
        // $penilaianDetail = Penilaian_Detail::all();
        // $id_detail = Penilaian_detail::where('id_penilaian_detail', '=', $request->id_penilaian_detail)->get();
        // // return $id_detail;
        // // Rekap::create($all);
        // return view ('rekapitulasi.simpan_sementara', [
        //     'nama_penilaian_detail' => $nama_penilaian_detail,
        //     'id_detail' => $id_detail
        // ]);
    }

    public function nama_penilaian_detail(Request $request){
        $id_penilaian_induk = $request->input('id_penilaian_induk');
        $nama_penilaian_detail = Penilaian_Detail::where('id_penilaian_induk', '=', $id_penilaian_induk)->get();
        return response()->json($nama_penilaian_detail);
    }

    public function ket_penilaian_detail(Request $request){
        $id_penilaian_detail = $request->input('id_penilaian_detail');
        $ket_penilaian_detail = Penilaian_Detail::where('id_penilaian_detail', '=', $id_penilaian_detail)->get();
        return response()->json($ket_penilaian_detail);
    }

    public function simpan_rekap(Request $request)
    {
        $all = $request->all();
        $id_penilaian_detail = $request->input('id_penilaian_detail');
        $list_rekapdetail = Penilaian_Detail::find($all['id_penilaian_detail']);

        $validasi = Validator::make($all, [
            'id_peserta' => 'required',
            'id_pegawai' => 'required|max:20',
            'id_penilaian_detail' => ['required', Rule::unique('rekap', 'id_penilaian_detail')->where(function($query) use ($request){
                                                        return $query->where('id_peserta', $request->id_peserta);
                                                    })],
            'penilaian_induk' => 'required',
            'nilai_detail' => 'required|numeric',
            'ket' => 'sometimes|max:200'
            ]);

            if ($validasi->fails()) {
                return back()->with('warning', $validasi->messages()->all()[0])->withInput();
            }else{
                $nilai = $request->nilai_detail;
                $nilai_akhir = ($list_rekapdetail['persentase_detail'] / 100) * $nilai;
                $request->request->add(['nilai_akhir' => $nilai_akhir]);
                // Rekap::create($all);
                $data = new Rekap;
                $data->id_penilaian_detail = $request->id_penilaian_detail;
                $data->nilai_detail = $request->nilai_detail;
                $data->id_pegawai = $request->id_pegawai;
                $data->id_peserta = $request->id_peserta;
                $data->ket = $request->ket;
                $data->nilai_akhir = $nilai_akhir;
                $data->nilai_detail = $request->nilai_detail;
                $data->save();
            }

        return redirect()->back()->with('toast_success', 'Nilai tersimpan');
    }

    public function update_sementara(Request $request)
    {
        $pelatihan = Pelatihan::all();
        // dump($pelatihan);
        return redirect()->route('rekapitulasi.show', $pelatihan->id_pelatihan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list_peserta = DB::table('peserta')
                            ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
                            ->WHERE('peserta.id_peserta', $id)
                            ->GET();
        $id_pelatihan = $list_peserta[0]->id_pelatihan;
        $list_pelatihan = Pelatihan::where('id_pelatihan', '=', "$id_pelatihan")->get();
        $id_jenis = $list_pelatihan[0]->id_jenis;
        $list_jenis = Jenis::where('id_jenis', '=', "$id_jenis")->get();
        $list_ = $list_jenis[0]->id_jenis;
        $list_penilaian_induk = Penilaian::where('id_jenis', '=', "$list_")->get()->toArray();

        $list_detail = DB::table('rekap')
                        ->JOIN('penilaian_detail','rekap.id_penilaian_detail', '=','penilaian_detail.id_penilaian_detail')
                        ->JOIN('penilaian_induk','penilaian_detail.id_penilaian_induk', '=','penilaian_induk.id_penilaian_induk')
                        ->orderBy('rekap.id_rekap','DESC')
                        ->WHERE('rekap.id_peserta', $id)
                        ->get();
        $list_wi = Pegawai::all();
        // dump($list_peserta);
        $list_agama = Agama::all();
        return view ('rekapitulasi.edit', [
            'list_peserta' => $list_peserta,
            'list_agama' => $list_agama,
            'list_wi' => $list_wi,
            'list_penilaian_induk' => $list_penilaian_induk,
            'list_detail' => $list_detail
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id;
        $list_detail = Rekap::find($id);
        // $list_detail->delete();
        return redirect()->back()->with('toast_info', "Data berhasil dihapus");
    }

    public function laporan_index()
    {
        $pelatihan = Pelatihan::all();
        // return $id_pelatihan;
        return view ('report.index', compact('pelatihan'));
    }

    public function index_peserta(Request $request)
    {
        $id_pelatihan = $request->get('id_pelatihan');
        // return $id_pelatihan;
        $nama_pelatihan = Pelatihan::findOrFail($id_pelatihan);
        $list_peserta = Peserta::where('id_pelatihan', $id_pelatihan)->get();

        $sql_kualifikasi = "SELECT peserta.id_peserta, peserta.nama_peserta, peserta.nip, peserta.tempat_lahir, peserta.tgl_lahir, peserta.jk, peserta.instansi, pelatihan.id_pelatihan, pelatihan.nama_pelatihan, SUM(rekap.nilai_akhir) AS 'TOTAL',
                            CASE
                            WHEN SUM(rekap.nilai_akhir) >= 90.01 THEN 'SANGAT MEMUASKAN'
                            WHEN SUM(rekap.nilai_akhir) >= 80.01 THEN 'MEMUASKAN'
                            WHEN SUM(rekap.nilai_akhir) >= 70.01 THEN 'BAIK'
                            WHEN SUM(rekap.nilai_akhir) >= 60.01 THEN 'KURANG BAIK'
                            ELSE 'NILAI BELUM LENGKAP'
                            END AS KUALIFIKASI
                            FROM peserta
                            LEFT JOIN rekap on peserta.id_peserta = rekap.id_peserta
                            LEFT JOIN pelatihan on pelatihan.id_pelatihan = peserta.id_pelatihan
                            WHERE pelatihan.id_pelatihan = $id_pelatihan
                            GROUP by peserta.id_peserta
                            ORDER BY TOTAL DESC";

        $table_kualifikasi = DB::select($sql_kualifikasi);
        // dump($table_kualifikasi);

        return view('report.index_peserta', compact('list_peserta', 'nama_pelatihan', 'table_kualifikasi','id_pelatihan'));
    }

    public function laporan(Request $request, $id)
    {
        // $id_peserta = $request->get('id_peserta');
        $rekap = Rekap::where('id_peserta', $id)->orderBy('id_rekap', 'ASC')->get();
        // return $rekap;
        $peserta = Peserta::all();

        $penilaian = Penilaian_Detail::all();
        // $list_pelatihan = Pelatihan::findOrFail($id);
        // $list_peserta1 = Peserta::where('id_peserta', $id_peserta)->get();

        $list_peserta1 = DB::table('rekap')
            ->JOIN('peserta','peserta.id_peserta', '=','peserta.id_peserta')
            ->JOIN('penilaian_detail','penilaian_detail.id_penilaian_detail', '=','penilaian_detail.id_penilaian_detail')
            ->orderBy('rekap.id_rekap','DESC')
            ->WHERE('peserta.id_peserta', $id)
            ->GET();


        // dump($list_peserta1);
        $jumlah = Rekap::where('id_peserta', $id)->select('nilai_akhir')->get();
        $collect = collect($jumlah)->sum('nilai_akhir');

        return view ('report.laporan', [
            'list_peserta1' => $list_peserta1,
            'penilaian' => $penilaian,
            'rekap' => $rekap,
            'collect' => $collect
        ]);

    }

    public function export($id)
    {
        $list_peserta1 = DB::table('rekap')
            ->JOIN('peserta','peserta.id_peserta', '=','peserta.id_peserta')
            ->JOIN('penilaian_detail','penilaian_detail.id_penilaian_detail', '=','penilaian_detail.id_penilaian_detail')
            ->WHERE('peserta.id_peserta', $id)
            ->GET();
        $id_pelatihan = Peserta::findOrFail($id);
        $pelatihan = Pelatihan::where('id_pelatihan',$id_pelatihan->id_pelatihan)->get();
        // return $pelatihan;
        $uri = Url::current();

        foreach ($pelatihan as $key) {
            $key->nama_pelatihan;
        }
        $rekap = Rekap::where('id_peserta', $id)->get();
        $jumlah = Rekap::where('id_peserta', $id)->select('nilai_akhir')->get();
        $persentase_detail = DB::table('rekap')
                                ->JOIN('penilaian_detail', 'penilaian_detail.id_penilaian_detail', '=','rekap.id_penilaian_detail')
                                ->WHERE('id_peserta', $id)
                                ->SELECT('persentase_detail')
                                ->GET();

        $collect_persen = collect($persentase_detail)->sum('persentase_detail');
        // return $collect_persen;

        $collect = collect($jumlah)->sum('nilai_akhir');
        if($collect >= 90.1)
        {
            $result = 'SANGAT MEMUASKAN';
        }elseif($collect >= 80.1)
        {
            $result = 'MEMUASKAN';
        }elseif($collect >= 70.1)
        {
            $result = 'BAIK';
        }elseif($collect >= 60.1)
        {
            $result = 'KURANG BAIK';
        }else
        {
            $result = 'Nilai belum lengkap';
        }
        $pdf = PDF::loadview('report.cetak', compact('rekap', 'list_peserta1', 'collect', 'key', 'result', 'collect_persen', 'uri'));
        $pdf->setPaper('folio', 'landscape');
        $namapdf = $list_peserta1[0]->nama_peserta;

        return $pdf->stream("Laporan Nilai - ".$namapdf.".pdf", array("Attatchment" => false));
    }

    public function export_pelatihan(Request $request, $id)
    {
        // $id_pelatihan = $request->get('id_pelatihan');
        $nama_pelatihan = Pelatihan::findOrFail($id);
        $uri = $request->fullUrl();

        // return $uri;
        // $list_peserta = Peserta::where('id_pelatihan', $id_pelatihan)->get();

        $sql_kualifikasi = "SELECT peserta.id_peserta, peserta.nama_peserta, peserta.nip, peserta.tempat_lahir, peserta.tgl_lahir, peserta.jk, peserta.instansi, pelatihan.id_pelatihan, pelatihan.nama_pelatihan, SUM(rekap.nilai_akhir) AS 'TOTAL',
                            CASE
                            WHEN SUM(rekap.nilai_akhir) > 90.01 THEN 'SANGAT MEMUASKAN'
                            WHEN SUM(rekap.nilai_akhir) > 80.01 THEN 'MEMUASKAN'
                            WHEN SUM(rekap.nilai_akhir) > 70.01 THEN 'BAIK'
                            WHEN SUM(rekap.nilai_akhir) > 60.01 THEN 'KURANG BAIK'
                            WHEN SUM(rekap.nilai_akhir) >= 0 THEN 'NILAI BELUM LENGKAP'
                            ELSE 'NILAI BELUM LENGKAP'
                            END AS KUALIFIKASI
                            FROM peserta
                            LEFT JOIN rekap on peserta.id_peserta = rekap.id_peserta
                            LEFT JOIN pelatihan on pelatihan.id_pelatihan = peserta.id_pelatihan
                            WHERE pelatihan.id_pelatihan = $id
                            GROUP by peserta.id_peserta
                            ORDER BY TOTAL DESC";

        $table_kualifikasi = DB::select($sql_kualifikasi);

        // foreach ($pelatihan as $key) {
        //     $key->nama_pelatihan;
        // }
        $rekap = Rekap::where('id_peserta', $id)->get();
        $jumlah = Rekap::where('id_peserta', $id)->select('nilai_akhir')->get();
        $collect = collect($jumlah)->sum('nilai_akhir');
        // return $collect;
        if($collect >= 90.1)
        {
            $result = 'Sangat Memuaskan';
        }elseif($collect >= 80.1)
        {
            $result = 'Memuaskan';
        }elseif($collect >= 70.1)
        {
            $result = 'Baik';
        }elseif($collect >= 60.1)
        {
            $result = 'Kurang Baik';
        }else
        {
            $result = 'Nilai belum lengkap';
        }
        $pdf = PDF::loadview('report.cetak_pelatihan', compact('rekap', 'table_kualifikasi', 'collect', 'result', 'nama_pelatihan','uri'));
        $pdf->setPaper('folio', 'landscape');
        $namapdf = $nama_pelatihan->nama_pelatihan;

        return $pdf->stream("Rekap Nilai - ".$namapdf.".pdf", array("Attatchment" => false));
    }
}
