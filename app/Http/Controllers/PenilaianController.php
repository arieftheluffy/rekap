<?php

namespace App\Http\Controllers;

use Validator;
use App\Penilaian;
use App\Jenis;
use App\Pelatihan;
use App\Penilaian_Detail;
use App\Rekap;
use App\RekapDetail;
use Illuminate\Http\Request;

class PenilaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_penilaian = Penilaian::orderBy('id_penilaian_induk','DESC')->get();
        $list_jenis_pelatihan = Jenis::all();
        return view ('penilaian.index', [
            'list_penilaian' => $list_penilaian,
            'list_jenis_pelatihan' => $list_jenis_pelatihan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_jenis_pelatihan = Jenis::all();
        $list_pelatihan = Pelatihan::all();
        return view ('penilaian.create', [
            'list_jenis_pelatihan' => $list_jenis_pelatihan,
            'list_pelatihan' => $list_pelatihan
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validasi = Validator::make($input, [
            'nama_penilaian_induk' => 'required|max:150',
            'persentase' => 'required'
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        // return $input;

        Penilaian::create($input);

        return redirect()->route('penilaian.index')->with('toast_success', 'Data berhasil disimpan!');
    }

    public function detail_index()
    {
        $list_penilaian_detail = Penilaian_Detail::orderBy('id_penilaian_detail', 'DESC')->get();
        return view ('penilaian.detail_index', [
            'list_penilaian_detail' => $list_penilaian_detail
        ]);
    }

    public function detail_create()
    {
        $list_penilaian_induk = Penilaian::all();
        $list_pelatihan = Pelatihan::all();
        return view ('penilaian.detail_create', [
            'list_penilaian_induk' => $list_penilaian_induk,
            'list_pelatihan' => $list_pelatihan
        ]);
    }

    public function detail_store(Request $request)
    {
        // return $request;
        $input = $request->all();

        $validasi = Validator::make($input, [
            'id_penilaian_induk' => 'required',
            'nama_penilaian_detail' => 'required|max:200',
            'persentase_detail' => 'required'
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        Penilaian_Detail::create($input);

        return redirect()->route('penilaian.detail_index')->with('toast_success', 'Data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

    }
    public function hapus_penilaian($id)
    {
        // return $id;
        $penilaian_induk = Penilaian::findOrFail($id);
        $penilaian_induk->delete();
        return back()->with('toast_success', 'Berhasil dihapus!');
    }

    public function hapus($id)
    {
        $penilaian_detail = Penilaian_Detail::findOrFail($id);
        $penilaian_detail->delete();
        $rekap = Rekap::where('id_rekap', $id)->delete();
        // $rekap_detail = RekapDetail::where('id_rekap', $id)->delete();
        // $penilaian_detail->penilaian_detail->whereIdPenilaianDetail->delete();
        return redirect('/penilaian/detail')->with('toast_success', 'Berhasil dihapus');
    }
}
