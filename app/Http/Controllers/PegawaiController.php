<?php

namespace App\Http\Controllers;

use App\Pegawai;
use App\Pangkat;
use App\Jabatan;
use Validator;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_pegawai = Pegawai::all();
        return view ('setting.pegawai.index',[
            'list_pegawai' => $list_pegawai,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_pegawai = Pegawai::all();
        $list_pangkat = Pangkat::all();
        $list_jabatan = Jabatan::all();
        return view ('setting.pegawai.create',[
            'list_pegawai' => $list_pegawai,
            'list_pangkat' => $list_pangkat,
            'list_jabatan' => $list_jabatan,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validasi = Validator::make($input, [
            'nama_pegawai' => 'required|min:4|max:100',
            'nip' => 'required|numeric',
            'tempat_lahir' => 'required|max:100',
            'tgl_lahir' => 'required|date',
            'jk' => 'required',
            'unit_kerja' => 'required|max:100',
            'instansi' => 'required|max:100',
            'id_pangkat' => 'required|numeric',
            'id_jabatan' => 'required|numeric',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        // return $input;
        Pegawai::create($input);
        return redirect()->route('pegawai.index')->with('toast_success','Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pegawai = Pegawai::findOrFail($id);
        $list_pangkat = Pangkat::all();
        $list_jabatan = Jabatan::all();
        return view ('setting.pegawai.edit', compact('pegawai', 'list_pangkat', 'list_jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $input = $request->all();

        $validasi = Validator::make($input, [
            'nama_pegawai' => 'required|min:4|max:100',
            'nip' => 'required|numeric',
            'tempat_lahir' => 'required|max:100',
            'tgl_lahir' => 'required|date',
            'jk' => 'required',
            'unit_kerja' => 'required|max:100',
            'instansi' => 'required|max:100',
            'id_pangkat' => 'required|numeric',
            'id_jabatan' => 'required|numeric',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        // return $input;
        $data = Pegawai::findOrFail($id);
        $data->nama_pegawai = $request->nama_pegawai;
        $data->nip = $request->nip;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->tgl_lahir = $request->tgl_lahir;
        $data->jk = $request->jk;
        $data->instansi = $request->unit_kerja;
        $data->id_pangkat = $request->id_pangkat;
        $data->id_jabatan = $request->id_jabatan;
        $data->save();

        return redirect()->route('pegawai.index')->with('toast_success','Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return $id;
        // $pegawai_id = Pegawai::find($id);
        // $pegawai_id->delete();
        // return redirect()->back()->with('toast_success', "Data berhasil dihapus");
    }


    public function hapus_data($id)
    {
        // return $id;
        $pegawai_id = Pegawai::find($id);
        $pegawai_id->delete();
        return redirect()->back()->with('toast_success', "Data berhasil dihapus");
    }
}
