<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelatihan;
use App\Peserta;
use App\Jenis;
use App\Lokasi;
use Validator;
use DB;
use App\Imports\PesertaImport;
use Maatwebsite\Excel\Facades\Excel;

class PelatihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_pelatihan = Pelatihan::all();
        $list_lokasi = Lokasi::all();
        $list_jenis = Jenis::all();
        return view ('pelatihan.index', [
            'list_pelatihan' => $list_pelatihan,
            'list_lokasi' => $list_lokasi,
            'list_jenis' => $list_jenis,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_pelatihan = Pelatihan::all();
        $list_jenis = Jenis::all();
        $list_lokasi = Lokasi::all();
        return view ('pelatihan.create', [
            'list_pelatihan' => $list_pelatihan,
            'list_lokasi' => $list_lokasi,
            'list_jenis' => $list_jenis,
        ]);
    }

    public function updatedata(Request $request)
    {
        $input = $request->all();
        dd($input);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validasi = Validator::make($input, [
            'nama_pelatihan' => 'required|min:4|max:200',
            'jp' => 'required|min:200|max:1000|numeric',
            'ket' => 'sometimes|max:200',
            'tgl_mulai' => 'required|date',
            'tgl_akhir' => 'required|date',
            'kuota' => 'required|numeric|min:1|max:45',
            'tahun' => 'required|numeric|min:2019',
            'id_jenis' => 'required|numeric',
            'id_lokasi' => 'required|numeric',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        // dump($input);

        Pelatihan::create($input);
        return redirect()->route('pelatihan.index')->with('toast_success','Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list_pelatihan = Pelatihan::find($id);

        $list_peserta = DB::table('peserta')
                            ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
                            ->WHERE('pelatihan.id_pelatihan', $id)
                            ->GET();
        // dump($list_peserta);

        return view ('pelatihan.show', [
            'list_peserta' => $list_peserta,
            'list_pelatihan' => $list_pelatihan,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list_pelatihan = Pelatihan::findOrFail($id);
        return view ('pelatihan.editdata', [
            'list_pelatihan' => $list_pelatihan,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        // return $input;
        $validasi = Validator::make($input, [
            'nama_pelatihan' => 'required|min:4|max:200',
            'jp' => 'required|min:200|max:1000|numeric',
            'ket' => 'sometimes|max:200',
            'tgl_mulai' => 'required|date',
            'tgl_akhir' => 'required|date',
            'kuota' => 'required|numeric|min:1|max:45',
            'tahun' => 'required|numeric|min:2019',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        $pelatihan = Pelatihan::findOrFail($request->id_pelatihan);
        $pelatihan->update($request->all());
        return back()->with('toast_success','Data berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id;
    }

    public function import(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'import_file' => 'required|mimes:xls,xlsx'
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }

        try {
            Excel::import(new PesertaImport, request()->file('import_file'));
        }
        catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }
            if ($e->failures()) {
                return back()->with('warning', $failure->errors())->withInput();
            }
        }

        return redirect()->route('pelatihan.show', $request->id_pelatihan)->with('toast_success', 'Import berhasil!');
    }
}
