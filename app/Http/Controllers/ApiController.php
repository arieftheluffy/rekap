<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rekap;
use App\Penilaian_Detail;

class ApiController extends Controller
{
    public function editnilai(Request $request, $id)
    {

        // return $request->all();
        $rekap = Rekap::find($id);

        $nilai = $request->value;
        $list_rekapdetail = Penilaian_Detail::find($request->name);
        $nilai_akhir = ($list_rekapdetail['persentase_detail'] / 100) * $nilai;
        $rekap->update([
            'nilai_detail' => $request->value,
            'nilai_akhir' => $nilai_akhir
        ]);

        return response()->json($rekap);

    }

}
