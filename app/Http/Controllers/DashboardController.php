<?php

namespace App\Http\Controllers;

use App\Pelatihan;
use App\Peserta;
use DB;
use App\Rekap;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $pelatihan = Pelatihan::all();
        $peserta = Peserta::all();

        // $collect = DB::table('rekap')
        //                 ->JOIN('peserta','peserta.id_peserta', '=','rekap.id_peserta')
        //                 ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
        //                 ->select(DB::raw('rekap.id_peserta as id_pes'), DB::raw('sum(nilai_akhir) as totalnilaibypeserta'), DB::raw('peserta.id_peserta, peserta.nama_peserta, peserta.nip, peserta.instansi'), DB::raw('pelatihan.id_pelatihan, pelatihan.nama_pelatihan'))
        //                 ->groupBy(DB::raw('rekap.id_peserta'))
        //                 ->orderBy('totalnilaibypeserta', 'DESC')
        //                 ->get();
        $sql = "SELECT rekap.id_peserta AS id_pes, peserta.nama_peserta, peserta.id_peserta, peserta.nip, peserta.instansi, pelatihan.id_pelatihan, pelatihan.nama_pelatihan, SUM(rekap.nilai_akhir) AS 'totalnilaibypeserta'
                FROM rekap
                INNER JOIN peserta ON peserta.id_peserta = rekap.id_peserta
                INNER JOIN pelatihan ON peserta.id_pelatihan = pelatihan.id_pelatihan
                GROUP BY rekap.id_peserta
                ORDER BY totalnilaibypeserta DESC";
        $collect2 = DB::select($sql);
        // return $collect2;
        return view ('dashboard', compact('pelatihan', 'peserta', 'collect2'));
    }
}
