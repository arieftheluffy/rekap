<?php

namespace App\Http\Controllers;

use App\Agama;
use Illuminate\Http\Request;
use App\Peserta;
use App\Pangkat;
use App\Pelatihan;
use DB;
use Validator;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_peserta = Peserta::all();
        return view ('peserta.index', [
            'list_peserta' => $list_peserta
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list_pelatihan = Pelatihan::all();
        $list_peserta = Peserta::find($id);

        return view ('peserta.show', [
            'list_peserta' => $list_peserta,
            'list_pelatihan' => $list_pelatihan,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list_peserta = DB::table('peserta')
                            ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
                            ->WHERE('peserta.id_peserta', $id)
                            ->GET();
        // dump($list_peserta);
        $list_pangkat = Pangkat::all();
        $list_agama = Agama::all();
        return view ('pelatihan.edit', [
            'list_peserta' => $list_peserta,
            'list_agama' => $list_agama,
            'list_pangkat' => $list_pangkat,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $list_peserta = DB::table('peserta')
                            ->JOIN('pelatihan','peserta.id_pelatihan', '=','pelatihan.id_pelatihan')
                            ->WHERE('peserta.id_peserta', $id)
                            ->GET();

        $list = Peserta::find($id);
        $input = $request->all();

        $validasi = Validator::make($input, [
            'nama_peserta' => 'required|min:4|max:100',
            'nip' => 'required|min:18|max:18',
            'nik' => 'required|min:16|max:16',
            'tempat_lahir' => 'required',
            'id_agama' => 'required',
            'id_pangkat' => 'required',
            'tgl_lahir' => 'required|date',
            'alamat' => 'required|max:200',
            'tlp' => 'required|numeric',
            'instansi' => 'required|max:100',
            'ket' => 'sometimes|max:200',
        ]);

        if ($validasi->fails()) {
            return back()->with('warning', $validasi->messages()->all()[0])->withInput();
        }
        // return $list;
        $list->update($input);

        return redirect()->route('pelatihan.show', $list_peserta[0]->id_pelatihan)->with('toast_success', 'Data berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
