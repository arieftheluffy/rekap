<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PelatihanResource;
use App\Pelatihan;
use App\Peserta;
use Validator;
use Illuminate\Http\Request;

class PelatihanController extends Controller
{
    public function index()
    {
        return PelatihanResource::collection(Pelatihan::all());
    }

    public function get_peserta(Request $request)
    {
        $id_pelatihan = $request->input('id_pelatihan');
        $peserta = Peserta::where('id_pelatihan', $id_pelatihan)->get();

        $validasi = Validator::make($request->all(), [
            'id_pelatihan'=>'required',
        ]);

        if ($validasi->fails()) {
            return response()->json([
                'status'=>FALSE,
                'message'=>$validasi->errors()
            ], 400);
        }else if ($peserta->isEmpty()) {
            return response()->json([
                'status'=>FALSE,
                'message'=>'Data tidak ditemukan'
            ], 200);
        }else{
            return response()->json([
                'status'=>TRUE,
                'message'=>$peserta
            ], 200);
        }


        // return response()->json($peserta);
    }
}
