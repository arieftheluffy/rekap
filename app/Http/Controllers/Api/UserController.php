<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Http\Resources\UserResource;
use App\User;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'status' => FALSE,
                'message' => $validator->errors()
            ], 400);
        }

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where([
            ['email', $email]
        ])->first();

        if(password_verify($password, $user->password))
            {
                return response()->json([
                    'status' => TRUE,
                    'message' => 'Login Berhasil!',
                    'user' => new UserResource($user)
                ], 200);
            }else
            {
                return response()->json([
                    'status' => FALSE,
                    'message' => 'Email/Password tidak sesuai!',
                ], 200);
            }
    }
}
