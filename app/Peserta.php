<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $table = "peserta";
    protected $primaryKey = "id_peserta";
    protected $dates = ['tgl_lahir'];
    protected $fillable = [
        'nama_peserta', 'nip', 'id_agama','id_pangkat', 'nik', 'tempat_lahir', 'alamat', 'tlp', 'instansi', 'tgl_lahir', 'jk', 'id_pelatihan'
    ];

    public function agama()
    {
        return $this->belongsTo('App\Agama', 'id_agama');
    }

    public function pelatihan()
    {
        return $this->belongsTo('App\Pelatihan', 'id_pelatihan');
    }

}
