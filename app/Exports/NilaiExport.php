<?php

namespace App\Exports;

use App\Rekap;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NilaiExport implements FromQuery, WithHeadings
{

    use Exportable;

       /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return Peserta::where('id_pelatihan', $this->id)->get();
    // }
    public function query()
    {
        return Rekap::query()->where('id_peserta', 6);
    }

    public function headings(): array
    {
        return [
            '#',
            'Nama Unsur Penilaian',
            'NIP',
        ];
    }
}
