<?php

namespace App\Imports;

use App\Peserta;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PesertaImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        // dd($row);
        return new Peserta([
            'id_pelatihan' => $row['id_pelatihan'],
            'nama_peserta' => $row['nama'],
            'nip' => $row['nip'],
            'nik' => $row['nomor_identitas'],
            'tempat_lahir' => $row['tempat_lahir'],
            'id_agama' => $row['agama'],
            'id_pangkat' => $row['pangkat'],
            'tgl_lahir' => $row['tanggal_lahir'],
            'alamat' => $row['alamat'],
            'tlp' => $row['no_telp_kantor'],
            'instansi' => $row['nama_instansi'],
            'jk' => $row['jenis_kelamin'],
        ]);
    }

    public function  rules(): array {
        return [
            'nip' => 'unique:peserta,nip',
        ];
    }
}
