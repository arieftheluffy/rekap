<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelatihan extends Model
{
    protected $table = 'pelatihan';
    protected $primaryKey = 'id_pelatihan';
    protected $fillable = [
        'nama_pelatihan', 'ket', 'id_lokasi','id_jenis', 'tgl_mulai', 'tgl_akhir', 'jp', 'kuota', 'tahun'
    ];

    public function jenis()
    {
        return $this->belongsTo('App\Jenis', 'id_jenis');
    }
}
