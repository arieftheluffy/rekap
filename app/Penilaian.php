<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penilaian extends Model
{
    protected $table = "penilaian_induk";
    protected $primaryKey = "id_penilaian_induk";
    protected $fillable = [
        'nama_penilaian_induk', 'ket_induk', 'persentase', 'id_jenis'
    ];

    public function jenis()
    {
        return $this->belongsTo('App\Jenis', 'id_jenis');
    }

    public function penilaian_detail()
    {
        return $this->hasMany('App\Penilaian_Detail', 'id_penilaian_induk');
    }
}
