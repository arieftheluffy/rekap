<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('rekapitulasi/{rekapitulasi}/editnilai', 'ApiController@editnilai');
Route::post('login_user', 'Api\UserController@login');
Route::get('get_pelatihan', 'Api\PelatihanController@index');
Route::post('get_peserta', 'Api\PelatihanController@get_peserta');