<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => ['auth', 'CheckRole:admin']], function () {
    Route::resource('pelatihan', 'PelatihanController');
    // Route::post('pelatihan/{pelatihan}/update', 'PelatihanController@updatedata')->name('updatedata');
    Route::resource('jabatan', 'JabatanController');
    Route::get('pegawai/{id}/hapus', 'PegawaiController@hapus_data');
    Route::resource('pegawai', 'PegawaiController')->except('show');
    Route::resource('peserta', 'PesertaController');
    // Route::post('rekapitulasi/simpan_sementara', 'RekapController@simpan_sementara')->name('rekapitulasi.simpan_sementara');
    Route::get('penilaian/detail', 'PenilaianController@detail_index')->name('penilaian.detail_index');
    Route::get('penilaian/detail/create', 'PenilaianController@detail_create')->name('penilaian.detail_create');
    Route::post('penilaian/detail', 'PenilaianController@detail_store')->name('penilaian.detail_store');
    Route::resource('penilaian', 'PenilaianController');
    Route::get('penilaian/detail/{penilaian}/delete', 'PenilaianController@hapus')->name('penilaian.nilai_hapus');
    Route::get('penilaian/{penilaian}/hapus', 'PenilaianController@hapus_penilaian')->name('penilaian.hapus_penilaian');
    // Route::post('rekapitulasi/update_sementara', 'RekapController@update_sementara')->name('rekapitulasi.update_sementara');
    Route::post('pelatihan/import', 'PelatihanController@import')->name('pelatihan.import');
    Route::resource('agama', 'AgamaController');
    Route::get('pangkat', 'PangkatController@index')->name('pangkat.index');
    Route::get('pangkat/create', 'PangkatController@create')->name('pangkat.create');
    Route::post('pangkat', 'PangkatController@store')->name('pangkat.store');
    Route::delete('pangkat/{pangkat}', 'PangkatController@destroy')->name('pangkat.destroy');
    Route::resource('jenis', 'JenisController');
    Route::resource('lokasi', 'LokasiController');
    // Route::get('/home', 'DashboardController@dashboard')->name('home');
});

Route::group(['middleware' => ['auth', 'CheckRole:admin,staff']], function () {
    Route::get('dashboard', 'DashboardController@dashboard');
    Route::get('rekapitulasi/{rekapitulasi}/edit/json-detail','RekapController@nama_penilaian_detail')->name('rekapitulasi.nama_penilaian_detail');
    Route::get('rekapitulasi/{rekapitulasi}/edit/json-ket','RekapController@ket_penilaian_detail')->name('rekapitulasi.ket_penilaian_detail');
    Route::get('rekapitulasi/create_nilai', 'RekapController@create_nilai')->name('rekapitulasi.create_nilai');
    Route::get('rekapitulasi/data_peserta/{rekapitulasi}', 'RekapController@data_peserta')->name('rekapitulasi.data_peserta');
    Route::get('rekapitulasi/data_peserta/{rekapitulasi}/nilai', 'RekapController@data_peserta_show')->name('rekapitulasi.data_peserta_show');
    Route::post('rekapitulasi/simpan_rekap', 'RekapController@simpan_rekap')->name('rekapitulasi.simpan_rekap');
    Route::resource('rekapitulasi', 'RekapController');
    Route::get('laporan/peserta', 'RekapController@index_peserta')->name('rekapitulasi.index_peserta');
    Route::get('laporan', 'RekapController@laporan_index')->name('rekapitulasi.laporan_index');
    Route::get('laporan/peserta/{laporan}/', 'RekapController@laporan')->name('rekapitulasi.laporan');
    Route::get('laporan/pelatihan/{id}/download', 'RekapController@export_pelatihan')->name('rekapitulasi.exportpelatihan');
    Route::get('laporan/{id}/download', 'RekapController@export')->name('rekapitulasi.export');
    Route::post('rekapitulasi/{rekapitulasi}/editnilai', 'ApiController@editnilai');
});