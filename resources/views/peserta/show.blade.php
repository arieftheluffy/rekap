@extends('layouts.template')

@section('content')
<h2 class="h3 mb-4 text-gray-800">Data Peserta Pelatihan</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    @foreach($list_pelatihan as $pelatihan)
                        <h5 class="h3 mb-4 text-gray-800">{{ $pelatihan->nama_pelatihan }}</h5>
                    @endforeach
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th width="5%">No.</th>
                        <th>Nama Peserta</th>
                        <th>NIP</th>
                        <th>Tempat, Tgl Lahir</th>
                        <th>Tlp</th>
                        <th>JK</th>
                        <th>Pangkat/Gol</th>
                        <th>Instansi</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($list_peserta as $peserta)
                        <tr>
                          <th>{{ $loop->iteration }}</th>
                          <th>{{ $peserta->nama_peserta }}</th>
                          <th>{{ $peserta->nip }}</th>
                          <th>{{ $peserta->tempt_lahir }}, {{ $peserta->tgl_lahir }}</th>
                          <th>{{ $peserta->tlp }}</th>
                          <th>{{ $peserta->jk }}</th>
                          <th>{{ $peserta->agama->nama_agama }}</th>
                          <th>{{ $peserta->instansi }}</th>
                          <th>
                              <a href="{{ route('peserta.index') }}" class='btn btn-sm btn-success'><i class='fa fa-users'></i></a>
                          </th>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
    </div>
@endsection