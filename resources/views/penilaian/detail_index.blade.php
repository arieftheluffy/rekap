@extends('layouts.template')

@section('css')
<!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Data Penilaian Induk</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{ route('penilaian.index') }}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Back</span>
                </a>
                <a href="{{ route('penilaian.detail_create')}}" class="btn btn-primary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-arrow-right"></i>
                  </span>
                  <span class="text">Tambah Detail Penilaian</span>
                </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th>Detail Penilaian</th>
                      <th>Persentase Detail</th>
                      <th>Nama Induk</th>
                      <th>Ket</th>
                      <th width="10%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($list_penilaian_detail as $penilaian_detail)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <th>{{ $penilaian_detail->nama_penilaian_detail }}</th>
                            <th width="5%">{{ $penilaian_detail->persentase_detail }}</th>
                            <th width="15%">{{ $penilaian_detail->penilaian_induk->nama_penilaian_induk }}</th>
                            <th width="35%">{{ $penilaian_detail->ket }}</th>
                            <th>
                                <a href="{{ route('penilaian.nilai_hapus', [$penilaian_detail->id_penilaian_detail]) }}" nama-detail="{{ $penilaian_detail->nama_penilaian_detail }}" class="btn btn-danger btn-sm delete-confirm">Delete</a>
                            </th>
                        </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')
<!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
      $(document).ready(function() {
      $('#nilai_detail').DataTable( {
          "scrollX": true
          } );
      });
  </script>
  <script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        var nama_detail = $(this).attr('nama-detail');
        swal({
            title: 'Data akan dihapus?',
            text: "'"+ nama_detail +"' akan dihapus?",
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
  </script>
@endsection