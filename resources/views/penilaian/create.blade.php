@extends('layouts.template')

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Data Penilaian Induk</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('penilaian.index')}}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                  <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Kembali</span>
              </a>
            </div>
            <div class="card-body">
                <form action="{{ route('penilaian.store') }}" method="POST" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="nama_penilaian_induk" placeholder="Nama Penilaian Induk" maxlength="150" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" minlength="1" maxlength="3" pattern="[0-9]*" name="persentase" placeholder="Masukkan Nilai Persentase(cth: 10)" class="form-control">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <select name="id_jenis" id="id_jenis" class="form-control" >
                                @foreach($list_jenis_pelatihan as $list_jenis)
                                    <option value="{{ $list_jenis->id_jenis }}">{{ $list_jenis->jenis_pelatihan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea type="text" class="form-control" name="ket_induk" placeholder="Keterangan" rows="3"></textarea>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary mb-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection