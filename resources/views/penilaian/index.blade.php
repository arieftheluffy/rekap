@extends('layouts.template')

@section('css')
  <!-- Custom styles for this page -->
    <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Data Penilaian Induk</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{ route('penilaian.create')}}" class="btn btn-primary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-arrow-right"></i>
                  </span>
                  <span class="text">Tambah Penilaian Induk</span>
                </a>
                <a href="{{ route('penilaian.detail_index') }}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Detail Penilaian</span>
                </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="tabelpeserta" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th>Penilaian</th>
                      <th>Persentase</th>
                      <th>Jenis Pelatihan</th>
                      <th>Keterangan</th>
                      <th width="10%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($list_penilaian as $penilaian_induk)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <th>{{ $penilaian_induk->nama_penilaian_induk }}</th>
                            <th width="10%">{{ $penilaian_induk->persentase }}</th>
                            <th width="30%">{{ $penilaian_induk->jenis->jenis_pelatihan }}</th>
                            <th width="20%">{{ $penilaian_induk->ket_induk }}</th>
                            <th>
                                <a href="{{ route('penilaian.hapus_penilaian', [$penilaian_induk->id_penilaian_induk]) }}" nama-detail="{{ $penilaian_induk->nama_penilaian_induk }}" class="btn btn-danger btn-sm delete-confirm">Delete</a>
                            </th>
                        </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')
<!-- Page level plugins -->
    <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <script>
        $(document).ready(function() {
        $('#tabelpeserta').DataTable( {
            "scrollX": true
            } );
        });
    </script>
  <script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        var nama_detail = $(this).attr('nama-detail');
        swal({
            title: 'Data akan dihapus?',
            text: "'"+ nama_detail +"' akan dihapus?",
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
  </script>
@endsection