@extends('layouts.template')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Input Detail Penilaian</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('penilaian.detail_index')}}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                  <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Kembali</span>
              </a>
            </div>
            <div class="card-body">
                <form action="{{ route('penilaian.detail_store') }}" method="POST" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col-md-12">
                        <label for="nama_penilaian">Penilaian Induk</label>
                            <select name="id_penilaian_induk" id="nama_penilaian" class="form-control" >
                                @foreach($list_penilaian_induk as $list_induk)
                                    <option value="{{ $list_induk->id_penilaian_induk }}">{{ $list_induk->nama_penilaian_induk }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nama_penilaian_detail">Detail Penilaian</label>
                            <textarea type="text" class="form-control" name="nama_penilaian_detail" placeholder="Input Detail Penilaian" maxlength="200" rows="2" required></textarea>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="ket_penilaian_detail">Ket Penilaian</label>
                            <textarea type="text" class="form-control" name="ket" id="ket_penilaian_detail" placeholder="Keterangan Detail Penilaian" rows="3"></textarea>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                        <label for="persentase_penilaian">Detail Persentase</label>
                            <input type="text" minlength="1" maxlength="3" pattern="[0-9]*" name="persentase_detail" placeholder="Masukkan Nilai Persentase(cth: 10)" class="form-control">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary mb-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#nama_penilaian').select2();
        })
    </script>
@endsection