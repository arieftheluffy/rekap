@extends('layouts.template')

@section('css')

@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Lokasi Pelatihan</h2>
    <div class="row">
        <div class="col-md-8">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('lokasi.create')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Tambah Data</span>
                  </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th width="50%">Lokasi</th>
                      <th>Ket.</th>
                      <th width="5%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($list_lokasi as $lokasi)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <th>{{ $lokasi->nama_lokasi }}</th>
                            <th>{{ $lokasi->ket }}</th>
                            <th>
                                <form method="POST" action="{{ route('lokasi.destroy', [$lokasi->id_lokasi]) }}">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')

@endsection