@extends('layouts.template')

@section('css')
  <!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Data Pegawai</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('pegawai.create')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Tambah Data</span>
                  </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="pegawai" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th width="30%">Nama</th>
                      <th>NIP</th>
                      <th>JK</th>
                      <th>Tempat, Tgl Lahir</th>
                      <th width="10%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($list_pegawai as $pegawai)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <th>{{ $pegawai->nama_pegawai }}</th>
                            <th>{{ $pegawai->nip }}</th>
                            <th>{{ $pegawai->jk }}</th>
                            <th>{{ $pegawai->tempat_lahir }}, {{ \Carbon\Carbon::parse($pegawai->tgl_lahir)->locale('id')->isoFormat('LL') }}</th>
                            <th>
                                <a href="{{ action('PegawaiController@hapus_data', [$pegawai->id_pegawai])}}" nama-detail="{{ $pegawai->nama_pegawai }}" class="btn btn-danger btn-sm delete-confirm"><i class="fas fa-trash"></i></a>
                                <a href="{{ route('pegawai.edit', [$pegawai->id_pegawai]) }}" class="btn btn-sm btn-info"><i class='fa fa-magic'></i></a>
                            </th>
                        </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')
  <script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        var nama_detail = $(this).attr('nama-detail');
        swal({
            title: 'Data akan dihapus?',
            text: "'"+ nama_detail +"' akan dihapus?",
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
  </script>
  <script>
      $(document).ready(function() {
      $('#pegawai').DataTable( {
          "scrollX": true
          } );
      });
  </script>
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endsection