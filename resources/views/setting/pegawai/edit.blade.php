@extends('layouts.template')

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Input Pegawai</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('pegawai.index')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                  </a>
            </div>
            <div class="card-body">
                <form action="{{ route('pegawai.update', [$pegawai->id_pegawai]) }}" method="POST" class="form-horizontal">
                @csrf
                @method('put')
                    <div class="row">
                        <div class="col-md-12">
                        <label for="">Nama Pegawai</label><span class="text-danger"> *</span>
                            <input type="text" class="form-control" name="nama_pegawai" placeholder="Cth : Abdullah" value="{{ $pegawai->nama_pegawai }}" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">NIP</label><span class="text-danger"> *</span>
                            <input type="text" class="form-control" name="nip" minlength="18" maxlength="18" pattern="[0-9]*" value="{{ $pegawai->nip }}"placeholder="NIP Pegawai" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Jenis Kelamin</label><span class="text-danger"> *</span>
                            <select name="jk" id="jk" class="form-control">
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Pangkat/Golongan</label><span class="text-danger"> *</span>
                            <select name="id_pangkat" id="id_pangkat" class="form-control">
                                @foreach($list_pangkat as $pangkat)
                                    <option value="{{ $pangkat->id }}"@if($pegawai->id_pangkat == $pangkat->id) Selected @endif>{{ $pangkat->nama_pangkat }} ({{ $pangkat->golongan }})</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Jabatan</label><span class="text-danger"> *</span>
                            <select name="id_jabatan" id="id_pangkat" class="form-control">
                                @foreach($list_jabatan as $jabatan)
                                    <option value="{{ $jabatan->id_jabatan }}"@if($pegawai->id_jabatan == $jabatan->id_jabatan) Selected @endif>{{ $jabatan->nama_jabatan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Tempat Lahir</label><span class="text-danger"> *</span>
                            <input type="text" class="form-control" name="tempat_lahir" value="{{ $pegawai->tempat_lahir }}" placeholder="Tempat Lahir (cth: Kutai Kartanegara)" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Tgl Lahir</label><span class="text-danger"> *</span>
                            <input type="date" class="form-control" name="tgl_lahir" value="{{ $pegawai->tgl_lahir }}" placeholder="Tgl Lahir" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Unit Kerja</label><span class="text-danger"> *</span>
                            <input type="text" class="form-control" name="unit_kerja" value="{{ $pegawai->unit_kerja }}" placeholder="Unit Kerja" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Instansi</label><span class="text-danger"> *</span>
                            <input type="text" class="form-control" name="instansi" value="{{ $pegawai->instansi }}" placeholder="Instansi Induk" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary mb-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection