@extends('layouts.template')

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Pangkat dan Golongan</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('pangkat.index')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                  </a>
            </div>
            <div class="card-body">
                <form action="{{ route('pangkat.store') }}" method="POST" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="nama_pangkat" placeholder="Pangkat Pegawai">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="golongan" placeholder="Golongan Pegawai">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary mb-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection