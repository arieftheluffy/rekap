@extends('layouts.template')

@section('css')

@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Pangkat dan Golongan</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('pangkat.create')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Tambah Data</span>
                  </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th width="70%">Pangkat</th>
                      <th>Gol/Ruang</th>
                      <th width="10%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach($list_pangkat as $pangkat)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <th>{{ $pangkat->nama_pangkat }}</th>
                            <th>{{ $pangkat->golongan }}</th>
                            <th>
                                <form method="POST" action="{{ route('pangkat.destroy', [$pangkat->id]) }}">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </th>
                        </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')

@endsection