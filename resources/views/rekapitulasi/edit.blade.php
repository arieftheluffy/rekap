@extends('layouts.template')

@section('css')
    <link href="{{ asset('css/bootstrap-editable.css') }}" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Custom styles for this page -->
    <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
{{-- <h2 class="h3 mb-4 text-gray-800">Input Nilai Peserta</h2> --}}
    <div class="row">
        <div class="col-lg-6 col-xs-12">
            <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Peserta</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label col-sm-12">Kelas</label>
                        <div class="col-sm-12">
                            <input type="text" name="tempat_lahir" class="form-control" value="{{ $list_peserta[0]->nama_pelatihan }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12">Nama Peserta</label>
                        <div class="col-sm-12">
                            <input type="text" name="nama_peserta" class="form-control" value="{{ $list_peserta[0]->nama_peserta }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12">Instansi</label>
                        <div class="col-sm-12">
                            <input type="text" name="instansi" class="form-control" readonly value="{{ $list_peserta[0]->instansi }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12">NIP</label>
                        <div class="col-sm-12">
                            <input type="text" name="nip" class="form-control" value="{{ $list_peserta[0]->nip }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12">Alamat</label>
                        <div class="col-sm-12">
                            <textarea type="text" name="alamat" class="form-control" rows="4" readonly>{{ $list_peserta[0]->alamat }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                </div>
              </div>
        </div>
        <div class="col-lg-6 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Input Nilai Peserta</h6>
                </div>
                <div class="card-body">
                <form class="form-horizontal" method="post" action="{{ route('rekapitulasi.simpan_rekap') }}">
                @csrf
                    <input type="hidden" name="id_jenis" value="{{ $list_peserta[0]->id_jenis }}">
                    <input type="hidden" name="id_lokasi" value="{{ $list_peserta[0]->id_lokasi }}">
                    <input type="hidden" name="id_peserta" value="{{ $list_peserta[0]->id_peserta }}">
                    <div class="form-group">
                        <label class="control-label col-sm-6">Widyaiswara yang memberi nilai <span class="text-danger" title="This field is required">*</span></label>
                        <div class="col-sm-12">
                            <select name="id_pegawai" id="id_pegawai" class="form-control datawi">
                                <option disable="true" selected="true" required>=== Pilih Widyaiswara ===</option>
                                @foreach($list_wi as $in => $value)
                                    <option value="{{ $value['id_pegawai'] }}">{{ $value['nama_pegawai'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12">Penilaian Induk <span class="text-danger" title="This field is required">*</span></label>
                        <div class="col-sm-12">
                            <select name="penilaian_induk" id="penilaian_induk" class="form-control">
                                <option value="" disable="true" selected="true" required>=== Pilih Penilaian Utama ===</option>
                                @foreach($list_penilaian_induk as $in => $value)
                                    <option value="{{ $value['id_penilaian_induk'] }}">{{ $value['nama_penilaian_induk'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-12">Unsur Penilaian Detail <span class="text-danger" title="This field is required">*</span></label>
                        <div class="col-sm-12">
                            <select name="id_penilaian_detail" id="id_penilaian_detail" class="form-control">
                                    <option value=""  disable="true" selected="true" required>=== Pilih Unsur Penilaian Detail ===</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                        <label for="garis">Input Nilai</label><br>
                            <input type="text" minlength="2" maxlength="3" pattern="[0-9]*" name="nilai_detail" value="{{ old('nilai_detail')}}" placeholder="Masukkan Nilai (cth: 90)" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <h4 name="ket_penilaian_detail" id="ket_penilaian_detail" for="ket_penilaian_detail"></h4>
                        {{-- <label for="garis">=======</label><br> --}}
                        </div>
                    </div>
                </div>
                 <div class="card-footer">
                    <a href="{{ route('rekapitulasi.show', $list_peserta[0]->id_pelatihan) }}" class="btn btn-lg btn-secondary float-right">
                        <span class="icon text-white-50"></i></span><span class="text">Kembali</span>
                    </a>
                    <input type="submit" name="submit" value="Proses" class="btn btn-lg btn-success">
                </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Unsur Penilaian</h6>
                </div>
                <div class="card-body py3">
                    <table id="dataNilai" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No.</th>
                                <th width="20%">Unsur Penilaian Induk</th>
                                <th width="30%">Unsur Penilaian Detail</th>
                                <th width="35%">Nama Penilaian Detail</th>
                                <th width="10%">Nilai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list_detail as $list)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $list->nama_penilaian_induk }}</td>
                                <td>{{ $list->nama_penilaian_detail }}</td>
                                <td>{{ $list->ket }}</td>
                                <td><a href="#" class="nilai" data-type="number" data-name="{{ $list->id_penilaian_detail }}" data-pk="{{ $list->id_rekap }}" data-url="/rekap/api/rekapitulasi/{{ $list->id_rekap }}/editnilai" data-title="Masukan nilai terbaru">{{ $list->nilai_detail }}</a></td>
                                <td>
                                    <form class="delete-btn" nama-detail="{{ $list->nama_penilaian_detail }}" method="POST" action="{{ route('rekapitulasi.destroy', [$list->id_rekap]) }}">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-sm btn-danger delete-btn"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
        $('#dataNilai').DataTable( {
            "scrollX": true
            } );
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.datawi').select2();
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.12.0/dist/sweetalert2.all.min.js"></script>
    <script>
        $('.delete-btn').on('click', function(e) {
            e.preventDefault();
            var self = $(this);
            var nama_detail = $(this).attr('nama-detail');
            Swal.fire({
                title: 'Data akan dihapus?',
                text: "PENILAIAN : "+ nama_detail +"",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Hapus!'
            }).then((result) => {
                if (result.value) {
                    $('.delete-btn').submit();
                }
            })
        });
    </script>
    <script src="{{ asset('js/bootstrap-editable.min.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script> --}}
    <script type="text/javascript">
        $('#penilaian_induk').on('change', function(e){
          console.log(e);
          var id_penilaian_induk = e.target.value;
          $.get('edit/json-detail?id_penilaian_induk=' + id_penilaian_induk,function(data) {
            console.log(data);
            $('#id_penilaian_detail').empty();
            $('#id_penilaian_detail').append('<option value="0" disable="true" selected="true">=== Select Unsur Penilaian ===</option>');

            $.each(data, function(index, id_penilaian_detailObj){
              $('#id_penilaian_detail').append('<option value="'+ id_penilaian_detailObj.id_penilaian_detail +'">'+ id_penilaian_detailObj.nama_penilaian_detail +'</option>');
            })
          });
        });

        $('#id_penilaian_detail').on('change', function(e){
          console.log(e);
          var id_penilaian_detail = e.target.value;
          $.get('edit/json-ket?id_penilaian_detail=' + id_penilaian_detail,function(data) {
            console.log(data);
            $('#ket_penilaian_detail').empty();
            $('#ket_penilaian_detail').append('<label></label>');

            $.each(data, function(index, id_penilaian_detailObj){
              $('#ket_penilaian_detail').append('<h4>'+ id_penilaian_detailObj.ket +'</h4>');
            })
          });
        });

        $(document).ready(function() {
            $('.nilaia').editable();
        });
        $(document).ready(function() {
            $('.nilai').editable({ type:'input', tpl: '<input type="number" onKeyPress="if(this.value.length==4) return false;" minlength="2" maxlength="3" min="60" max="100" pattern="[0-9]*" required>' });
        });
    </script>
@endsection