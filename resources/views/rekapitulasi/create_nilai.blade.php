@extends('layouts.template')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Input Nilai Peserta Pelatihan</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Pilih Kelas</h6>
              {{-- <a href="{{ route('rekapitulasi.index')}}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                  <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Kembali</span>
              </a> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-light" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th width="5%">No.</th>
                        <th>Pelatihan</th>
                        <th>Tgl Mulai</th>
                        <th>Tgl Akhir</th>
                        <th>Kuota</th>
                        <th>Jenis</th>
                        <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_pelatihan as $pelatihan)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <th>{{ $pelatihan->nama_pelatihan }}</th>
                            <th>{{ \Carbon\Carbon::parse($pelatihan->tgl_mulai)->locale('id')->isoFormat('LL') }}</th>
                            <th>{{ \Carbon\Carbon::parse($pelatihan->tgl_akhir)->locale('id')->isoFormat('LL') }}</th>
                            <th>{{ $pelatihan->kuota }}</th>
                            <th>{{ $pelatihan->jenis->jenis_pelatihan }}</th>
                            <th>
                                <a href="{{ route('rekapitulasi.show', $pelatihan->id_pelatihan) }}" class='btn btn-sm btn-danger'>Pilih Kelas</a>
                            </th>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#id_peserta').select2();
        })
    </script>
@endsection