@extends('layouts.template')

@section('css')
<!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Data Penilaian Induk</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{ route('rekapitulasi.create')}}" class="btn btn-primary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-arrow-right"></i>
                  </span>
                  <span class="text">Input Penilaian</span>
                </a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th rowspan="3" style="text-align: center; vertical-align: middle;">No</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle;">Nama</th>
                            <th colspan="2" style="text-align: center; vertical-align: middle;">A1<br>(15%)</th>
                            <th style="text-align: center; vertical-align: middle;">Z1</th>
                            <th colspan="2" style="text-align: center; vertical-align: middle;">A2<br>(20%)</th>
                            <th style="text-align: center; vertical-align: middle;">Z1</th>
                            <th colspan="4" style="text-align: center; vertical-align: middle;">A3<br>(20%)</th>
                            <th style="text-align: center; vertical-align: middle;">Z1</th>
                            <th colspan="4" style="text-align: center; vertical-align: middle;">A4<br>(30 %)</th>
                            <th style="text-align: center; vertical-align: middle;">Z1</th>
                            <th colspan="2" style="text-align: center; vertical-align: middle;">A5<br>(15%)</th>
                            <th style="text-align: center; vertical-align: middle;">Kualifikasi</th>
                            <th style="text-align: center; vertical-align: middle;">Peringkat</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>2</td>
                            <td>1</td>
                            <td>3</td>
                            <td>4</td>
                            <td>1</td>
                            <td>5</td>
                            <td>6</td>
                            <td>7</td>
                            <td>8</td>
                            <td>1</td>
                            <td>9</td>
                            <td>10</td>
                            <td>11</td>
                            <td>12</td>
                            <td>1</td>
                            <td>13</td>
                            <td>14</td>
                            <td>15</td>
                            <td>16</td>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($list_rekap as $rekap)
                          <tr>
                            <th>{{ $loop->iteration ?? '' }}</th>
                            <th>{{ $rekap->peserta->nama_peserta }}</td>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>{{ $rekap->nilai }}</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection