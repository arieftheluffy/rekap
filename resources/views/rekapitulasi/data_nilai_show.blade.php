@extends('layouts.template')

@section('css')
<script src="https://code.highcharts.com/highcharts.js"></script>
  <!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<h2 class="h3 mb-4 text-gray-800">Daftar Detail Nilai Peserta by Nama</h2>
    <div class="row">
        <div class="col-md-12">
          <div class="card shadow mb-4">
              <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Nama Peserta : {{ $list_peserta1[0]->nama_peserta }} - {{ $list_peserta1[0]->nip }}</h6>
              </div>
              <div class="card-body">
                  <div id="chartnilai" style="width:100%; height:550px;"></div>
              </div>
          </div>
        </div>
        <div class="col-md-12">
            <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Nama Peserta : {{ $list_peserta1[0]->nama_peserta }} - {{ $list_peserta1[0]->nip }}</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <a href="{{ route('rekapitulasi.data_peserta', $list_pelatihan[0]->id_pelatihan)}}" class="btn btn-primary btn-icon-split">
                          <span class="icon text-white-50">
                            <i class="fas fa-arrow-left"></i>
                          </span>
                          <span class="text">Kembali</span>
                        </a>
                    </div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th width="5%">No.</th>
                        <th>Nama Penilaian Induk</th>
                        <th>Rincian Penilaian</th>
                        <th width="5%">Persentase Detail (%)</th>
                        <th width="5%">Nilai</th>
                        <th width="5%">Nilai Akhir</th>
                        {{-- <th width="5%">Aksi</th> --}}
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($rekap as $i)
                        <tr>
                          <th>{{ $loop->iteration }}</th>
                          <th>{{ $i->penilaiandetail->penilaian_induk->nama_penilaian_induk }}</th>
                          <th>{{ $i->penilaiandetail->nama_penilaian_detail }}</th>
                          <th>{{ $i->penilaiandetail->persentase_detail }}</th>
                          <th>{{ $i->nilai_detail }}</th>
                          <th>{{ $i->nilai_akhir }}</th>
                          {{-- <th>
                              <a href="{{ url('rekapitulasi/'.$i->id_rekap.'/edit') }}" class='btn btn-sm btn-success'>Edit</a>
                          </th> --}}
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
    </div>
@endsection

@section('js')
  <!--- highcharts -->
  <script type="text/javascript">
      Highcharts.chart('chartnilai', {
      chart: {
          type: 'line'
      },
      title: {
          text: 'Rekap Nilai Peserta'
      },
      subtitle: {
          text: 'Sumber : BPSDM Prov. Kaltim'
      },
      xAxis: {
          categories: {!! json_encode($kategori) !!},
          crosshair: true
      },
      yAxis: {
          min: 60,
          max: 100,
          title: {
              text: 'Nilai'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'Unsur Penilaian',
          data: {!! json_encode($datanilai) !!}

      }]
  });
  </script>
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection