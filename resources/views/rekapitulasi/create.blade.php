@extends('layouts.template')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

@endsection
@section('content')
    <h2 class="h3 mb-4 text-gray-800">Input Penilaian Peserta</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('rekapitulasi.index')}}" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                  <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Kembali</span>
              </a>
            </div>
            <div class="card-body">
                <form action="{{ route('rekapitulasi.store') }}" method="POST" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <select name="id_penilaian_induk" id="id_penilaian_induk" class="form-control" >
                                @foreach($list_penilaian_induk as $induk)
                                    <option value="{{ $induk->id_penilaian_induk }}">{{ $induk->nama_penilaian_induk }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <select name="id_penilaian_detail" id="id_penilaian_detail" class="form-control" >
                                @foreach($list_penilaian_detail as $detail)
                                    <option value="{{ $detail->id_penilaian_detail }}">{{ $detail->nama_penilaian_detail }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <select name="id_peserta" id="id_peserta" class="form-control" id="id_peserta" >
                                @foreach($list_peserta as $peserta)
                                    <option value="{{ $peserta->id_peserta }}">{{ $peserta->pelatihan->nama_pelatihan }} - {{ $peserta->nama_peserta }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <select name="id_pegawai" id="id_pegawai" class="form-control" >
                                @foreach($list_pegawai as $pegawai)
                                    <option value="{{ $pegawai->id_pegawai }}">{{ $pegawai->nama_pegawai }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="number" name="nilai" class="form-control" minlength="2" maxlength="3" pattern="[0-9]*" placeholder="Nilai Peserta (cth: 90)" required>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea type="text" class="form-control" name="ket" placeholder="Keterangan" rows="5"></textarea>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary mb-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#id_peserta').select2();
        })
    </script>
@endsection