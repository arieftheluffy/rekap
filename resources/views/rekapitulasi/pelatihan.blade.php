@extends('layouts.template')

@section('css')
  <!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<h2 class="h3 mb-4 text-gray-800">Input Nilai Peserta Pelatihan</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Kelas : {{ $list_pelatihan->nama_pelatihan }}</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <a href="{{ route('rekapitulasi.create_nilai')}}" class="btn btn-primary btn-icon-split">
                          <span class="icon text-white-50">
                            <i class="fas fa-arrow-left"></i>
                          </span>
                          <span class="text">Kembali</span>
                        </a>
                    </div>
                    <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th width="5%">No.</th>
                        <th>Nama Peserta</th>
                        <th>NIP</th>
                        <th>Tempat, Tgl Lahir</th>
                        <th>Instansi</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($list_peserta as $peserta)
                        <tr>
                          <th>{{ $loop->iteration }}</th>
                          <th>{{ $peserta->nama_peserta }}</th>
                          <th>{{ $peserta->nip }}</th>
                          <th>{{ $peserta->tempat_lahir }}, {{ \Carbon\Carbon::parse($peserta->tgl_lahir)->locale('id')->isoFormat('LL') }}</th>
                          <th>{{ $peserta->instansi }}</th>
                          <th>
                              <a href="{{ url('rekapitulasi/'.$peserta->id_peserta.'/edit') }}" class='btn btn-sm btn-danger'>Input Nilai</a>
                          </th>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
    </div>
@endsection

@section('js')
  <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection