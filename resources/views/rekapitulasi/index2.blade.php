@extends('layouts.template')

@section('css')
<!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Daftar Nilai Peserta - Pilih Kelas</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{ route('rekapitulasi.create_nilai')}}" class="btn btn-primary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-arrow-right"></i>
                  </span>
                  <span class="text">Input Penilaian</span>
                </a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th width="5%">No.</th>
                        <th>Pelatihan</th>
                        <th>Tgl Mulai</th>
                        <th>Tgl Akhir</th>
                        <th>Kuota</th>
                        <th>Jenis</th>
                        <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list_pelatihan as $pelatihan)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $pelatihan->nama_pelatihan }}</td>
                            <td>{{ \Carbon\Carbon::parse($pelatihan->tgl_mulai)->locale('id')->isoFormat('LL') }}</td>
                            <td>{{ \Carbon\Carbon::parse($pelatihan->tgl_akhir)->locale('id')->isoFormat('LL') }}</td>
                            <td>{{ $pelatihan->kuota }}</td>
                            <td>{{ $pelatihan->jenis->jenis_pelatihan }}</td>
                            <td>
                                <a href="{{ route('rekapitulasi.data_peserta', $pelatihan->id_pelatihan) }}" class='btn btn-sm btn-success'>Pilih Kelas</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection