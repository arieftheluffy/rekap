   <!DOCTYPE html>
   <html lang="en">
   <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Laporan Nilai - {{ $nama_pelatihan->nama_pelatihan }}</title>
   <style type="text/css">

    #watermark {
      position: fixed;
      /**
          Set a position in the page for your image
          This should center it vertically
      **/
      bottom:   16cm;
      left:     10cm;
      opacity: .05;
      /**transform: rotate(10deg);
      /** Change image dimensions**/
      width: 10px; height: 10px;

      /** Your watermark should be behind every content**/
      z-index:  -1000;
  }
   .centerjudul {
    text-align: center;
    font-family: Arial;
    font-size: 14px;
    font-style: bold;
  }
  body {
    font-family: Arial;
    font-style: normal;
    font-size: 12px;
    margin-right: 5mm;
    margin-left: 5mm;
    margin-top: 0mm;
    margin-button: 5mm;
  }
  .table {
    font-style: normal;
  }
  .warna {
    background: #FFF5EE;
  }
  .center {
    text-align: center;
  }
  .nilai {
    text-align: center;
    font-style: bold;
  }
  tr:nth-child(even) {
    background-color: #f2f2f2;
  }
   </style>
   </head>
   <body>
      <div id="watermark">
        <img src="{{ asset('template/img/provkaltimgreyscale.png')}}" alt="">
      </div>
      <div class="content">
      <p class="centerjudul header">DATA REKAPITULASI PELATIHAN <br>DI BADAN PENGEMBANGAN SUMBER DAYA MANUSIA PROVINSI KALIMANTAN TIMUR</p>
      <p>Nama Pelatihan : {{ $nama_pelatihan->nama_pelatihan }}</p>
      <p>Tgl Pelaksanaan : {{ \Carbon\Carbon::parse($nama_pelatihan->tgl_mulai)->locale('id')->isoFormat('LL') }} - {{ \Carbon\Carbon::parse($nama_pelatihan->tgl_akhir)->locale('id')->isoFormat('LL') }}</p>
          <table width="98%" border="1" cellpadding="5" cellspacing="0">
              <thead>
                <tr>
                    <th width="5%">Ranking</th>
                    <th width="20%">Nama Peserta</th>
                    <th width="15%">NIP</th>
                    <th width="15%">Tempat, Tgl Lahir</th>
                    <th width="5%">JK</th>
                    <th width="10%">Total Nilai</th>
                    <th width="15%" class="warna">Kualifikasi</th>
                    {{-- <th width="">Kualifikasi</th> --}}
                </tr>
              </thead>
              <tbody>
                @foreach($table_kualifikasi as $i)
                  <tr>
                    <td class="center">{{ $loop->iteration }}</td>
                    <td height="10" align="left">{{ $i->nama_peserta }}</td>
                    <td>{{ $i->nip }}</td>
                    <td>{{ $i->tempat_lahir }}, {{ \Carbon\Carbon::parse($i->tgl_lahir)->locale('id')->isoFormat('LL') }}</td>
                    <td class="center">{{ $i->jk }}</td>
                    <td class="center">{{ $i->TOTAL }}</td>
                    <td class="nilai warna">
                      @if ($i->KUALIFIKASI == 'SANGAT MEMUASKAN')
                        {{ $i->KUALIFIKASI }}
                      @elseif ($i->KUALIFIKASI == 'MEMUASKAN')
                        {{ $i->KUALIFIKASI }}
                      @elseif ($i->KUALIFIKASI == 'BAIK')
                        {{ $i->KUALIFIKASI }}
                      @else
                        {{ $i->KUALIFIKASI }}
                      @endif
                    </td>
                    {{-- <td></td> --}}
                  </tr>
                @endforeach
              </tbody>
            </table>
      </div>
      <script type="text/php">
        if ( isset($pdf) ) {
            $x = 30;
            $y = 580;
            $text = "Hal. {PAGE_NUM} dari {PAGE_COUNT}";
            $font = $fontMetrics->get_font("arial", "normal");
            $size = 9;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
      </script>
      <script type="text/php">
        if ( isset($pdf) ) {
            $x = 25;
            $y = 15;
            $text = "{{ url($uri) }}";
            $font = $fontMetrics->get_font("arial", "normal");
            $size = 9;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
      </script>
   </body>
   </html>