@extends('layouts.template')

@section('css')
<!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h3 class="h3 mb-4 text-gray-800">Nama Peserta : {{ $list_peserta1[0]->nama_peserta }} - {{ $list_peserta1[0]->nip }}</h3>
       <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
             <div class="card-header py-3">
                  <a href="{{ route('rekapitulasi.laporan_index') }}" class="btn btn-danger">Back</a>
                  <a target="_blank" href="{{ route('rekapitulasi.export', $list_peserta1[0]->id_peserta) }}" class="btn btn-success">Download</a>
              </div>

            <div class="card-body">
               <table class="table table-striped" id="nilai" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th>Nama Penilaian Induk</th>
                        <th>Rincian Penilaian</th>
                        <th width="5%">Nilai</th>
                        <th width="5%">Persentase Detail (%)</th>
                        <th width="5%">Nilai Akhir</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($rekap as $i)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $i->penilaiandetail->penilaian_induk->nama_penilaian_induk }}</td>
                        <td>{{ $i->penilaiandetail->nama_penilaian_detail }}</td>
                        <td>{{ $i->nilai_detail }}</td>
                        <td>{{ $i->penilaiandetail->persentase_detail }}</td>
                        <td>{{ $i->nilai_akhir }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
            <div class="card-footer">
              <h6 class="h6 font-weight-bold text-primary">TOTAL NILAI : {{ $collect }}</h6>
            </div>
          </div>
        </div>
@endsection

@section('js')
  <script>
      $(document).ready(function() {
      $('#nilai').DataTable( {
          "scrollX": true
          } );
      });
  </script>
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
  <!--- Button datatable -->
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

@endsection