@extends('layouts.template')

@section('css')
<!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <h5 class="h5 mb-4 text-gray-800">{{ $nama_pelatihan->nama_pelatihan }}</h5>
       <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
             <div class="card-header py-3">
                  <a href="{{ route('rekapitulasi.laporan_index') }}" class="btn btn-danger">Back</a>
                  <a target="_blank" href="{{ route('rekapitulasi.exportpelatihan', $id_pelatihan) }}" class="btn btn-success">Download</a>
              </div>

            <div class="card-body">
               <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                          <th width="5%">Ranking</th>
                          <th width="22%">Nama Peserta</th>
                          <th width="12%">NIP</th>
                          <th width="17%">Tempat, Tgl Lahir</th>
                          {{-- <th width="20%">Instansi</th> --}}
                          <th width="5%">JK</th>
                          <th width="7%">Nilai Total</th>
                          <th width="12%">Kualifikasi</th>
                          <th width="7%">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($table_kualifikasi as $i)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $i->nama_peserta }}</td>
                          <td>{{ $i->nip }}</td>
                          <td>{{ $i->tempat_lahir }}, {{ \Carbon\Carbon::parse($i->tgl_lahir)->locale('id')->isoFormat('LL') }}</th>
                          {{-- <th>{{ $i->instansi }}</th> --}}
                          <td>{{ $i->jk }}</td>
                          <td>{{ round($i->TOTAL,3) }}</td>
                          <td>
                              @if ($i->KUALIFIKASI == 'SANGAT MEMUASKAN')
                                <span class="badge badge-primary bg-success">{{ $i->KUALIFIKASI }}</span>
                              @elseif ($i->KUALIFIKASI == 'MEMUASKAN')
                                <span class="badge badge-primary bg-INFO">{{ $i->KUALIFIKASI }}</span>
                              @elseif ($i->KUALIFIKASI == 'BAIK')
                              <span class="badge badge-primary bg-warning">{{ $i->KUALIFIKASI }}</span>
                              @else
                                <span class="badge badge-primary bg-danger">{{ $i->KUALIFIKASI }}</span>
                              @endif</td>
                          <td>
                                <a href="{{ route('rekapitulasi.laporan', $i->id_peserta) }}" class='btn btn-sm btn-info'>Detail Nilai</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
          </div>
        </div>
@endsection

@section('js')
  <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection