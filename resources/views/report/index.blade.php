@extends('layouts.template')

@section('css')
<!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Nilai Peserta</h6>
            </div>
            <div class="card-body">
                <form method="GET" action="{{ route('rekapitulasi.index_peserta') }}">
                <div class="row">
                    <div class="col-lg-10">
                        <select name="id_pelatihan" class="form-control">
                            @foreach($pelatihan as $p)
                                <option value="{{ $p->id_pelatihan }}">{{ $p->nama_pelatihan }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="form-control float-right btn btn-success">Pilih Pelatihan</button>
                    </div>
                </div>
                </form>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection