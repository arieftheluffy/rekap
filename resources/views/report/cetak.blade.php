   <!DOCTYPE html>
   <html lang="en">
   <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Laporan Nilai - {{ $list_peserta1[0]->nama_peserta }}</title>
   <style type="text/css">

    #watermark {
      position: fixed;
      /**
          Set a position in the page for your image
          This should center it vertically
      **/
      bottom:   16cm;
      left:     10cm;
      opacity: .05;
      /**transform: rotate(10deg);
      /** Change image dimensions**/
      width: 10px; height: 10px;

      /** Your watermark should be behind every content**/
      z-index:  -1000;
  }
   .centerjudul {
    text-align: center;
    font-family: Arial;
    font-size: 14px;
    font-style: bold;
  }
  body {
    font-family: Arial;
    font-style: normal;
    font-size: 12px;
    margin-right: 5mm;
    margin-left: 5mm;
    margin-top: 0mm;
    margin-button: 0mm;
  }
  .table {
    font-style: normal;
  }
  .warna {
    background: #FFF5EE;
  }
  .center {
    text-align: center;
  }
  pre {
    text-family: Arial;
    font-size: 12px
  }
   </style>
   </head>
   <body>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <div id="watermark">
        <img src="{{ asset('template/img/provkaltimgreyscale.png')}}" alt="">
      </div>
      <div class="content">
      <p class="centerjudul">DAFTAR NILAI PESERTA PELATIHAN<br>DI BADAN PENGEMBANGAN SUMBER DAYA MANUSIA</p>
      <pre>
        Nama      : {{ $list_peserta1[0]->nama_peserta }}
        NIP       : {{ $list_peserta1[0]->nip }}
        Kelas     : {{ $key->nama_pelatihan }}
      </pre>
        <table width="100%" border="1" cellpadding="4" cellspacing="0">
          <thead>
            <tr>
                <th width="3%">No.</th>
                <th width="25%">Nama Penilaian Induk</th>
                <th width="40%">Rincian Penilaian</th>
                <th width="5%" class="warna">Nilai</th>
                <th width="5%">Persentase Detail (%)</th>
                <th width="7%" class="warna">Nilai Akhir</th>
            </tr>
          </thead>
          <tbody>
            @foreach($rekap as $i)
              <tr>
                <td class="center">{{ $loop->iteration }}</td>
                <td align="left">{{ $i->penilaiandetail->penilaian_induk->nama_penilaian_induk }}</td>
                <td align="left">{{ $i->penilaiandetail->nama_penilaian_detail }}</td>
                <td class="warna center">{{ $i->nilai_detail }}</td>
                <td class="center">{{ $i->penilaiandetail->persentase_detail }}</td>
                <td class="center warna">{{ $i->nilai_akhir }}</td>
              </tr>
            @endforeach
              <tr>
                <th colspan="6"></th>
              </tr>
              <tr>
                <th colspan="3" align="center" class="warna">TOTAL PERSENTASE</th>
                <th colspan="3" class="warna">{{ $collect_persen }} %</th>
                {{-- <th class="warna"></th> --}}
              </tr>
              <tr>
                <th colspan="3" align="center" class="warna">TOTAL AKUMULASI NILAI</th>
                <th colspan="3" class="warna">{{ $collect }}</th>
              </tr>
              <tr>
                <th colspan="3" align="center" class="warna">KUALIFIKASI</th>
                {{-- <th class="warna"></th> --}}
                <th colspan="3" class="warna">{{ $result }}</th>
              </tr>
          </tbody>
        </table>
      </div>
      <script type="text/php">
        if ( isset($pdf) ) {
            $x = 30;
            $y = 580;
            $text = "Hal. {PAGE_NUM} dari {PAGE_COUNT}";
            $font = $fontMetrics->get_font("arial", "normal");
            $size = 9;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
      </script>
      <script type="text/php">
        if ( isset($pdf) ) {
            $x = 25;
            $y = 15;
            $text = "{{ url($uri)}}";
            $font = $fontMetrics->get_font("arial", "normal");
            $size = 9;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
      </script>
   </body>
   </html>