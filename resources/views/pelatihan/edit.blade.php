@extends('layouts.template')

@section('content')
<h2 class="h3 mb-4 text-gray-800">Data Peserta Pelatihan</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Kelas : {{ $list_peserta[0]->nama_pelatihan }}</h6>
                </div>
                <form class="form-horizontal" method="post" action="{{ route('peserta.update', [$list_peserta[0]->id_peserta]) }}">
                @csrf
                @method('put')
                    <div class="card-body">
                        <input type="hidden" name="id_jenis" value="{{ $list_peserta[0]->id_jenis }}">
                        <input type="hidden" name="id_lokasi" value="{{ $list_peserta[0]->id_lokasi }}">
                        <div class="form-group">
                            <label class="control-label col-sm-2">Nama Peserta <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" name="nama_peserta" placeholder="Nama Peserta" class="form-control" value="{{ $list_peserta[0]->nama_peserta }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">NIP <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" name="nip" placeholder="NIP Peserta" class="form-control" value="{{ $list_peserta[0]->nip }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">NIK <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" name="nik" placeholder="Nama Peserta" class="form-control" value="{{ $list_peserta[0]->nik }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tempat Lahir <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" name="tempat_lahir" placeholder="Tempat Lahir" class="form-control" value="{{ $list_peserta[0]->tempat_lahir }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tgl Lahir <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="date" name="tgl_lahir" placeholder="Tgl Lahir" class="form-control" value="{{ $list_peserta[0]->tgl_lahir }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">JK <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <select name="jk" id="jk" class="form-control">
                                    <option value="L" @if($list_peserta[0]->jk == 'L') Selected @endif >Laki-Laki</option>
                                    <option value="P" @if($list_peserta[0]->jk == 'P') Selected @endif >Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Pangkat/Golongan <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <select name="id_pangkat" id="pangkat" class="form-control">
                                @foreach($list_pangkat as $pangkat)
                                    <option value="{{ $pangkat->id }}" @if($list_peserta[0]->id_pangkat == $pangkat->id ) Selected @endif >{{ $pangkat->nama_pangkat }} - {{ $pangkat->golongan }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Agama <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <select name="id_agama" id="agama" class="form-control">
                                    @foreach($list_agama as $aqidah)
                                        <option value="{{ $aqidah->id }}" @if($list_peserta[0]->id_agama == $aqidah->id) Selected @endif>{{ $aqidah->nama_agama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Alamat <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control" name="alamat" placeholder="Alamat Lengkap" rows="2">{{ $list_peserta[0]->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Telp <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" name="tlp" placeholder="Telp" class="form-control" value="{{ $list_peserta[0]->tlp }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Instansi <span class="text-danger" title="This field is required">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" name="instansi" placeholder="Instansi" class="form-control" value="{{ $list_peserta[0]->instansi }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <a href='{{ route('pelatihan.show', $list_peserta[0]->id_pelatihan) }}' class="btn btn-danger"><i class="fa fa-chevron-circle-left"></i>
                                    Kembali</a>
                                <input type="submit" name="submit" value="Simpan" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                </form>
              </div>
        </div>
    </div>
@endsection