@extends('layouts.template')

@section('css')
  <!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<h2 class="h3 mb-4 text-gray-800">Data Peserta Pelatihan</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Kelas : {{ $list_pelatihan->nama_pelatihan }}</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <a href="{{ route('pelatihan.index')}}" class="btn btn-primary btn-icon-split">
                          <span class="icon text-white-50">
                            <i class="fas fa-arrow-left"></i>
                          </span>
                          <span class="text">Kembali</span>
                        </a>
                            <button class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#importModal">
                              <span class="icon text-white-50">
                                <i class="fas fa-file-import"></i>
                              </span>
                              <span class="text">Import</span>
                            </button>
                    </div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th width="5%">No.</th>
                        <th>Nama Peserta</th>
                        <th>NIP</th>
                        <th>Tempat, Tgl Lahir</th>
                        <th>Tlp</th>
                        <th>JK</th>
                        <th>Instansi</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($list_peserta as $peserta)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $peserta->nama_peserta }}</td>
                          <td>{{ $peserta->nip }}</td>
                          <td>{{ $peserta->tempat_lahir }}, {{ \Carbon\Carbon::parse($peserta->tgl_lahir)->locale('id')->isoFormat('LL') }}</td>
                          <td>{{ $peserta->tlp }}</td>
                          <td>{{ $peserta->jk }}</td>
                          <td>{{ $peserta->instansi }}</td>
                          <td>
                              <a href="{{ url('peserta/'.$peserta->id_peserta.'/edit') }}" class='btn btn-sm btn-success'><i class='fa fa-user-edit'></i></a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
    </div>


  <div class="modal fade" id="importModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title"><strong>Import</strong> Data Peserta</h4>
              </div>
              <form class="form-horizontal" method="post" action="{{ route('pelatihan.import') }}" enctype="multipart/form-data">
                @csrf
                  <div class="modal-body">
                  <input type="hidden" name="id_pelatihan" id="id_pelatihan" value="{{ $list_pelatihan->id_pelatihan }}">
                      <div class="form-group">
                          <label for="import_file" class="control-label">File (XLS/XLSX):</label>
                          <input type="file" id="import_file" name="import_file" class="form-control" required>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                      <button type="submit" class="btn btn-primary">Import</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
@endsection

@section('js')
    <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection