@extends('layouts.template')

@section('content')
    <h2 class="h3 mb-4 text-gray-800">Data Pelatihan</h2>
    <div class="row">
        <div class="col-md-6">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="{{ route('pelatihan.index')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Kembali</span>
                  </a>
            </div>
            <div class="card-body">
                <form action="{{ route('pelatihan.store')}}" method="POST" class="form-horizontal">
                @csrf
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="nama_pelatihan" placeholder="Nama Pelatihan">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                            <select name="id_jenis" class="form-control"  id="id_jenis">
                                <option value="">-- Pilih Jenis Pelatihan --</option>
                                @foreach($list_jenis as $jenis)
                                    <option value="{{ $jenis->id_jenis }}">{{ $jenis->jenis_pelatihan }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8">
                            <select name="id_lokasi" class="form-control"  id="id_lokasi">
                                <option value="">-- Pilih Lokasi Pelatihan --</option>
                                @foreach($list_lokasi as $lokasi)
                                    <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="kuota" placeholder="Kuota">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="jp" placeholder="Total Jam Pelajaran">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="tahun" placeholder="Tahun">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="date" class="form-control" name="tgl_mulai" placeholder="Tanggal Mulai Pelatihan">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <input type="date" class="form-control" name="tgl_akhir" placeholder="Tanggal Berakhir Pelatihan">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea type="text" class="form-control" name="ket" placeholder="Keterangan" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary mb-2">Simpan</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    @include('sweetalert::alert')
@endsection