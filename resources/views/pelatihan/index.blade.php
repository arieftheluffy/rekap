@extends('layouts.template')

@section('css')
  <!-- Custom styles for this page -->
  <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('content')
  <h2 class="h3 mb-4 text-gray-800">Data Pelatihan</h2>
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a href="{{ route('pelatihan.create')}}" class="btn btn-primary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-arrow-right"></i>
                  </span>
                  <span class="text">Tambah Data</span>
                </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">No.</th>
                      <th>Pelatihan</th>
                      <th>Tgl Mulai</th>
                      <th>Tgl Akhir</th>
                      <th>Kuota</th>
                      <th>Jenis</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($list_pelatihan as $pelatihan)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $pelatihan->nama_pelatihan }}</td>
                        <td>{{ \Carbon\Carbon::parse($pelatihan->tgl_mulai)->locale('id')->isoFormat('LL') }}</td>
                        <td>{{ \Carbon\Carbon::parse($pelatihan->tgl_akhir)->locale('id')->isoFormat('LL') }}</td>
                        <td>{{ $pelatihan->kuota }}</td>
                        <td>{{ $pelatihan->jenis->jenis_pelatihan }}</td>
                        <td>
                            <a href="{{ route('pelatihan.show', $pelatihan->id_pelatihan) }}" class='btn btn-sm btn-success'><i class='fa fa-users'></i></a>
                            <a href="#" data-tahun="{{ $pelatihan->tahun }}" data-jp="{{ $pelatihan->jp }}" data-namapelatihan="{{ $pelatihan->nama_pelatihan }}" data-tglmulai="{{$pelatihan->tgl_mulai}}" data-tglakhir="{{$pelatihan->tgl_akhir}}" data-idpelatihan="{{ $pelatihan->id_pelatihan }}" data-kuota="{{ $pelatihan->kuota }}" data-jenispelatihan="{{ $pelatihan->jenis->jenis_pelatihan }}" class='btn btn-sm btn-info' data-toggle="modal" data-target="#modalEditpelatihan"><i class='fa fa-magic'></i></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div>

    <!-- Modal -->
  <div class="modal fade" id="modalEditpelatihan" tabindex="-1" role="dialog" aria-labelledby="modalEditpelatihan" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalEditpelatihan">Edit Pelatihan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form class="form-horizontal" method="post" action="{{ route('pelatihan.update', [$pelatihan->id_pelatihan]) }}">
          @method('patch')
          @csrf
        <div class="modal-body">
              <input type="hidden" name="id_pelatihan" value="" id="idpelatihan">
              <div class="form-group">
                  <label class="control-label col-sm-8">Nama Pelatihan <span class="text-danger" title="This field is required">*</span></label>
                  <div class="col-sm-12">
                      <input type="text" class="form-control" id="namapelatihan" name="nama_pelatihan">
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-8">Tgl Mulai <span class="text-danger" title="This field is required">*</span></label>
                  <div class="col-sm-12">
                      <input type="date" class="form-control" id="tglmulai" name="tgl_mulai">
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-8">Tgl Akhir <span class="text-danger" title="This field is required">*</span></label>
                  <div class="col-sm-12">
                      <input type="date" class="form-control" id="tglakhir" name="tgl_akhir">
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-8">Kuota <span class="text-danger" title="This field is required">*</span></label>
                  <div class="col-sm-12">
                      <input type="text" class="form-control" id="kuota" name="kuota">
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-8">JP <span class="text-danger" title="This field is required">*</span></label>
                  <div class="col-sm-12">
                      <input type="text" class="form-control" id="jp" name="jp">
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-sm-8">Tahun <span class="text-danger" title="This field is required">*</span></label>
                  <div class="col-sm-12">
                      <input type="text" class="form-control" id="tahun" name="tahun">
                  </div>
              </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('js')
{{-- <script>
    $('.delete-confirm').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('action');
        var nama_detail = $(this).attr('nama-detail');
        swal({
            title: 'Data akan dihapus?',
            text: "'"+ nama_detail +"' akan dihapus?",
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });
  </script>  --}}
<script>
$('#modalEditpelatihan').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var namapelatihan = button.data('namapelatihan')
  var tglmulai = button.data('tglmulai')
  var tglakhir = button.data('tglakhir')
  var kuota = button.data('kuota')
  var id_pelatihan = button.data('idpelatihan')
  var tahun = button.data('tahun')
  var jp = button.data('jp')

  var modal = $(this)
  modal.find('.modal-body #namapelatihan').val(namapelatihan)
  modal.find('.modal-body #tglmulai').val(tglmulai)
  modal.find('.modal-body #tglakhir').val(tglakhir)
  modal.find('.modal-body #kuota').val(kuota)
  modal.find('.modal-body #idpelatihan').val(id_pelatihan)
  modal.find('.modal-body #tahun').val(tahun)
  modal.find('.modal-body #jp').val(jp)
})
</script>
  <!-- Page level plugins -->
  <script src="{{ asset('template/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('template/js/demo/datatables-demo.js') }}"></script>
@endsection